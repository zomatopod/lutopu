import pytest

import mock_requests as mock_requests_
import lutopu.net


@pytest.fixture
def mock_requests(request):
    # Set the mock requests access token to the correct testing oauth
    # token and reset it when the test function completes.
    def teardown():
        lutopu.net.access_token = None
    lutopu.net.access_token = 54321
    request.addfinalizer(teardown)
    return mock_requests_.MockRequests()

