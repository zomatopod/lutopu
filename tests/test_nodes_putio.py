import json
import pytest

try:
    from unittest import mock
except ImportError:
    import mock

import lutopu.exceptions
from lutopu.nodes import putio


class TestFetchData(object):
    def test_instance(self, mock_requests):
        file_data = mock_requests.get_filelist_data()[0]

        node = putio.FetchData(file_data)
        assert node.id == file_data['id']
        assert node.data_type == file_data['data_type']
        assert node.data is file_data


class TestDirectoryNode(object):
    @pytest.fixture
    def dir_node(self, mock_requests):
        return putio.DirectoryNode(mock_requests.get_parent_data())

    def test_instance(self, mock_requests):
        # Default DirectoryNode.
        node = putio.DirectoryNode()
        assert node.id == 0
        assert node.identity == "files"
        assert node.data is None

        # DirectoryNode with data.
        dir_data = mock_requests.get_parent_data()
        node = putio.DirectoryNode(dir_data)
        assert node.id == dir_data['id']
        assert node.identity == dir_data['name']
        assert node.data is dir_data

    def test_build(self, dir_node, mock_requests):
        # Test correct building based on the 123 test directory defined
        # in the mock_requests module.
        with mock.patch("requests.get", new=mock_requests.get):
            dir_node.build()
            subnodes = dir_node.subnodes()
            assert len(subnodes) == 3
            assert isinstance(subnodes[0], putio.FetchData)
            assert isinstance(subnodes[1], putio.FetchData)
            assert isinstance(subnodes[2], putio.DirectoryNode)
            assert subnodes[0].id == 6546533
            assert subnodes[1].id == 7645645
            assert subnodes[2].id == 115932951

    def test_build_keeping_nodes(self, dir_node, mock_requests):
        # When the DirectoryNode fetches file(s) that it already has as
        # subnodes, it should always keep the existing nodes.
        with mock.patch("requests.get", new=mock_requests.get):
            dir_node.build()
            file1, file2, subdir = dir_node.subnodes()
            dir_node.build()
            assert file1 is dir_node.subnodes()[0]
            assert file2 is dir_node.subnodes()[1]
            assert subdir is dir_node.subnodes()[2]

    def test_build_added_nodes(self, dir_node, mock_requests):
        # Same as test_build_keeping_nodes(), except with adding new
        # nodes.
        with mock.patch("requests.get", new=mock_requests.get):
            # Only builds the first two child nodes.
            mock_requests.data_to_use['files'] = (
                mock_requests.data_to_use['files'][:2]
            )
            dir_node.build()
            file1, file2 = dir_node.subnodes()

            # Build all three child nodes.
            mock_requests.data_to_use = mock_requests.get_full_data()
            dir_node.build()
            assert len(dir_node.subnodes()) == 3
            assert file1 is dir_node.subnodes()[0]
            assert file2 is dir_node.subnodes()[1]
            assert (dir_node.subnodes()[2] is not file1 and
                    dir_node.subnodes()[2] is not file2)

    def test_build_removed_nodes(self, dir_node, mock_requests):
        # Same as test_build_keeping_nodes(), except with removing
        # existing nodes.
        with mock.patch("requests.get", new=mock_requests.get):
            # All three child nodes.
            dir_node.build()
            file1, file2, subdir = dir_node.subnodes()

            # Last two nodes.
            mock_requests.data_to_use['files'] = (
                mock_requests.data_to_use['files'][1:]
            )
            dir_node.build()
            assert len(dir_node.subnodes()) == 2
            assert file2 is dir_node.subnodes()[0]
            assert subdir is dir_node.subnodes()[1]

    def test_build_renamed_nodes(self,  dir_node, mock_requests):
        # Incoming files with the same id but a different name --
        # renamed items -- will require new nodes.
        with mock.patch("requests.get", new=mock_requests.get):
            dir_node.build()
            file1, file2, subdir = dir_node.subnodes()

            mock_requests.data_to_use['files'][1]['name'] = "Renamed.mkv"
            dir_node.build()
            assert file1 is dir_node.subnodes()[0]
            assert file2 is not dir_node.subnodes()[1]
            assert subdir is dir_node.subnodes()[2]

    def test_build_error(self, dir_node, mock_requests):
        dir_node.id = 1

        with mock.patch("requests.get", new=mock_requests.get):
            with pytest.raises(lutopu.exceptions.BuildError):
                dir_node.build()

    def test_add_subnode(self, dir_node, mock_requests):
        # Ensure explicitly added nodes are not overwritten when
        # building.
        files = mock_requests.get_filelist_data()

        with mock.patch("requests.get", new=mock_requests.get):
            # Build last two nodes and manually add the first one.
            mock_requests.data_to_use['files'] = (
                mock_requests.data_to_use['files'][:2]
            )
            dir_node.build()
            dir_node.add_subnode(putio.DirectoryNode(files[2]))
            file1, file2, added_node = dir_node.subnodes()

            # Build all three nodes. None should be overwritten.
            mock_requests.data_to_use = mock_requests.get_full_data()
            dir_node.build()
            assert file1 is dir_node.subnodes()[0]
            assert file2 is dir_node.subnodes()[1]
            assert added_node is dir_node.subnodes()[2]

    def test_remove_subnode(self, dir_node, mock_requests):
        # Ensure explicitly removed nodes are not kept when building.
        with mock.patch("requests.get", new=mock_requests.get):
            # Build all three nodes and remove the last one.
            dir_node.build()
            file1, file2, removed_node = dir_node.subnodes()
            dir_node.remove_subnode(removed_node)

            # Last node should be new.
            dir_node.build()
            assert file1 is dir_node.subnodes()[0]
            assert file2 is dir_node.subnodes()[1]
            assert removed_node is not dir_node.subnodes()[2]

    def test_create_directory(self, dir_node, mock_requests):
        with mock.patch("requests.post", new=mock_requests.post):
            dir_node.create_directory("NewFolder")
            assert isinstance(dir_node.subnodes()[0], putio.DirectoryNode)
            assert dir_node.subnodes()[0].id == 12345

    def test_delete(self, dir_node, mock_requests):
        # Delete should delete locally and remotely.
        with mock.patch("requests.get", new=mock_requests.get):
            dir_node.build()
            file1, file2, subdir = dir_node.subnodes()

            with mock.patch("requests.post", new=mock_requests.post):
                dir_node.delete(file2)
                # Test local.
                assert len(dir_node.subnodes()) == 2
                assert file1 in dir_node.subnodes()
                assert file2 not in dir_node.subnodes()
                assert subdir in dir_node.subnodes()
                # Test remote
                dir_node.build()
                assert len(dir_node.subnodes()) == 2
                assert file1 in dir_node.subnodes()
                assert file2 not in dir_node.subnodes()
                assert subdir in dir_node.subnodes()

    def test_delete_local_fail(self, dir_node, mock_requests):
        # A directory should not delete a node that is not one of its
        # subnodes.
        with mock.patch("requests.get", new=mock_requests.get):
            dir_node.build()
            file1, file2, subdir = dir_node.subnodes()
        with pytest.raises(ValueError):
            subdir.delete(file1)

    def test_delete_remote_fail(self, dir_node, mock_requests):
        # If the directory tries to delete a file that has already been
        # removed from put.io, it must still remove that node from its
        # own branch.
        with mock.patch("requests.get", new=mock_requests.get):
            dir_node.build()
            file1, file2, subdir = dir_node.subnodes()

            with mock.patch("requests.post", new=mock_requests.post):
                dir_node.delete(file2)
                # Re-add the node locally -- the file2 no longer exists
                # remotely and the net-fetch will raise an error.
                dir_node.add_subnode(file2)
                with pytest.raises(lutopu.exceptions.RequestError):
                    dir_node.delete(file2)
                # file2 nonetheless should be removed from dir_node.
                assert file1 in dir_node.subnodes()
                assert file2 not in dir_node.subnodes()
                assert subdir in dir_node.subnodes()

    def test_move(self, dir_node, mock_requests):
        # When moving, the local directory nodes should update
        # themselves locally as well as updating remotely.
        with mock.patch("requests.get", new=mock_requests.get):
            dir_node.build()
            file1, file2, subdir = dir_node.subnodes()
            with mock.patch("requests.post", new=mock_requests.post):
                dir_node.move(file1, subdir)
                # dir_node tests remote.
                dir_node.build()
                assert file1 not in dir_node.subnodes()
                assert file2 in dir_node.subnodes()
                # subdir tests local.
                assert file1 in subdir.subnodes()
                assert file2 not in subdir.subnodes()

    def test_move_local_fail(self, dir_node, mock_requests):
        # A directory should not move a node that is not one of its
        # subnodes.
        with mock.patch("requests.get", new=mock_requests.get):
            dir_node.build()
            file1, file2, subdir = dir_node.subnodes()
        with pytest.raises(ValueError):
            subdir.move(file1, subdir)

    def test_rename(self, dir_node, mock_requests):
        # Directory should rename locally and remotely.
        with mock.patch("requests.get", new=mock_requests.get):
            dir_node.build()
            file1, file2, subdir = dir_node.subnodes()

            new_name = "My New File.mkv"
            # To test correct identity transform.
            test_node = putio.FetchData(file1.data)
            test_node.identity = new_name

            with mock.patch("requests.post", new=mock_requests.post):
                dir_node.rename(file1, new_name)
                # Test local.
                assert file1.data['name'] == new_name
                assert file1.identity == test_node.identity
                # Test remote.
                dir_node.build()
                assert file1.data['name'] == new_name
                assert file1.identity == test_node.identity

    def test_rename_local_fail(self, dir_node, mock_requests):
        # A directory should not rename a node that is not one of its
        # subnodes.
        with mock.patch("requests.get", new=mock_requests.get):
            dir_node.build()
            file1, file2, subdir = dir_node.subnodes()
        with pytest.raises(ValueError):
            subdir.rename(file1, "A new name")


class TestTransfersNode(object):
    def test_instance(self):
        transfers = putio.TransfersNode()
        assert transfers.identity == "transfers"

    def test_build(self, mock_requests):
        with mock.patch("requests.get", new=mock_requests.get):
            transfers = putio.TransfersNode()
            transfers.build()
            assert len(transfers.subnodes()) == 1


class TestPutioRoot(object):
    def test_activate(self):
        def check_inst(nodes, class_):
            return any(isinstance(n, class_) for n in nodes)

        root = putio.PutioRoot()
        assert len(root.subnodes()) == 0
        root.activated = True
        assert check_inst(root.subnodes(), putio.TransfersNode)
        assert check_inst(root.subnodes(), putio.DirectoryNode)

    def test_reset(self):
        root = putio.PutioRoot()
        root.reset()
        assert len(root.subnodes()) == 0

        root.activated = True
        subnode = putio.DirectoryNode({'id':12, 'name':"A_Subdir"})
        root.find_subnode("files").add_subnode(subnode)

        assert root.find_subnode("files").find_subnode("A_Subdir") is subnode
        root.reset()
        assert len(root.subnodes()) == 2
        assert root.find_subnode("files").find_subnode("A_Subdir") is None


