import pytest
import time
import collections

try:
    from unittest import mock
except ImportError:
    import mock

import lutopu.exceptions
from lutopu import nodes
from lutopu.nodes import base
from lutopu.nodes import nav


class TestNode(object):
    """Test :class:`lutopu.nodes.Node`."""

    @pytest.fixture
    def node(self):
        # Test node class that defines a couple of autobuild test
        # methods and an overridden build() that tracks the number of
        # times it has been built.
        class _TestNode(nodes.Node):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                self.build_count = 0

            def build(self):
                self.build_count += 1

            @base.autobuild
            def std_autobuild_method(self):
                pass

            @base.autobuild
            def od_autobuild_method(self):
                raise base.Autobuilder.OnDemand()

        return _TestNode("node-1")

    def test_instance(self, node):
        assert node.parent is None
        assert not node.autobuild
        assert not node.ondemand

    def test_get_root(self, tree):
        # All nodes should have the root node as their `root` property.
        # The root of the root is itself.
        root = nodes.Node("root")
        node1 = nodes.Node("node-1")
        node2 = nodes.Node("node-2")
        node2.parent = node1
        node1.parent = root
        assert node2.root is root
        assert node1.root is root
        assert root.root is root

    def test_read_identity(self, node):
        assert node.identity == "node-1"

    def test_set_identity(self, node):
        node.identity = "node-2"
        assert node.identity == "node-2"

    def test_identity_normalize(self, node):
        node.identity = "node number 3"
        assert node.identity == "node_number_3"

    def test_no_autobuild(self, node):
        # Autobuilding requires the autobuild attribute to be true.
        for _ in range(5):
            node.std_autobuild_method()
            node.od_autobuild_method()
        assert node.build_count == 0

    def test_autobuild_firsttime(self, node):
        # When autobuilding, only the first @autobuild method called
        # should initiate the build.
        node.autobuild = True
        for _ in range(5):
            node.std_autobuild_method()
            node.od_autobuild_method()
        assert node.build_count == 1

    def test_force_build(self, node):
        # After an explicit build, no first-time autobuilds should take
        # place (only stale and on-demand builds, tested later).
        node.autobuild = True
        node.build()
        assert node.build_count == 1
        for _ in range(5):
            node.std_autobuild_method()
            node.od_autobuild_method()
        assert node.build_count == 1

    def test_ondemand_build_clear(self, node):
        # After build(), the ondemand attribute should always be
        # cleared.
        node.ondemand = True
        node.build()
        assert node.ondemand == False

    def test_autobuild_ondemand(self, node):
        # An on-demand build can be initiated once, and only once, after
        # an initial node (auto)build if the ondemand attribute is
        # true.
        node.build()                    # Build #1.

        node.autobuild = node.ondemand = True
        node.od_autobuild_method()      # Build #2.
        node.od_autobuild_method()      # This should not build.
        assert node.build_count == 2

    def test_autobuild_stale(self, node):
        # A node whose build time is older than its optional
        # build_timeout_secs attribute will be autobuilt.
        node.build()                    # Build #1.

        node.autobuild = True
        node.build_timeout_secs = 0.25
        time.sleep(0.3)
        node.std_autobuild_method()     # Build #2.
        node.std_autobuild_method()     # This should not build.
        time.sleep(0.3)
        node.std_autobuild_method()     # Build #3.
        assert node.build_count == 3


class TestNodeCollection(object):
    """Test :class:`lutopu.nodes.NodeCollection`."""

    parent = None
    child1 = None
    child2 = None

    @pytest.fixture
    def collection(self):
        self.parent = nodes.Node("parent")
        self.child1 = nodes.Node("child-1")
        self.child2 = nodes.Node("child-2")

        self.parent.autobuild = True
        self.parent.ondemand = False
        # These attributes are not part of Node.
        self.parent.new_attr = "assign"
        self.parent.new_unassign_attr = "do not assign"

        nc = nodes.NodeCollection(self.parent)
        nc.assign_attrs = ["autobuild", "ondemand", "new_attr"]
        nc.append(self.child1)
        nc.append(self.child2)

        return nc

    def test_append(self):
        parent = nodes.Node("parent")
        child = nodes.Node("child-1")
        nc = nodes.NodeCollection(parent)

        nc.append(child)
        assert child.parent is parent

    def test_assign_attrs(self, collection):
        # Attributes of the parent node defined in assign_attrs should
        # be set to child nodes.
        parent, child1, child2 = self.parent, self.child1, self.child2
        assert child1.autobuild == child2.autobuild == parent.autobuild
        assert child1.ondemand == child2.ondemand == parent.ondemand
        assert child1.new_attr == child2.new_attr == parent.new_attr
        assert not hasattr(child1, "new_unassign_attr")
        assert not hasattr(child2, "new_unassign_attr")

    def test_length(self, collection):
        collection = collection
        assert len(collection) == 2

    def test_index(self, collection):
        assert collection[0] is self.child1
        assert collection[1] is self.child2

    def test_index_out_of_range(self, collection):
        with pytest.raises(IndexError):
            collection[2]
        with pytest.raises(IndexError):
            collection[-3]

    def test_contains(self, collection):
        assert self.child1 in collection
        assert self.child2 in collection

    def test_contains_not(self, collection):
        node = nodes.Node("new node")
        assert node not in collection


    def test_remove(self, collection):
        collection.remove(self.child1)
        assert self.child1 not in collection
        assert self.child2 in collection
        assert len(collection) == 1

    def test_remove_invalid_node(self, collection):
        with pytest.raises(ValueError):
            node = nodes.Node("new node")
            collection.remove(node)

    def test_find(self, collection):
        child2 = self.child2
        assert collection.find(child2.identity) is child2

    def test_find_fail(self, collection):
        assert collection.find("null node") is None

    def test_clear(self, collection):
        collection = collection
        child1, child2 = self.child1, self.child2

        collection.clear()
        assert len(collection) == 0
        assert child1 not in collection
        assert child2 not in collection
        assert child1.parent is None
        assert child2.parent is None

    def test_iterator(self, collection):
        itr = iter(collection)
        assert next(itr) is self.child1
        assert next(itr) is self.child2
        with pytest.raises(StopIteration):
            next(itr)


class TestTreeNode(object):
    """Test basic :class:`lutopu.nodes.TreeNode` functionality."""

    sub1 = None
    sub2 = None

    @pytest.fixture
    def tree(self):
        TestTreeNode.sub1 = nodes.TreeNode("subnode-1")
        TestTreeNode.sub2 = nodes.TreeNode("subnode-2")

        # Test node class that defines an overriddeen build() that
        # attaches subnodes. The first call to build will attach sub1,
        # the second attaches sub2, and further calls will have no
        # effect except incrementing the build counter.
        class _TestTreeNode(nodes.TreeNode):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                self.build_count = 0

            def build(self):
                if self.build_count == 0:
                    self.add_subnode(TestTreeNode.sub1)
                elif self.build_count == 1:
                    self.add_subnode(TestTreeNode.sub2)
                self.build_count += 1

        return _TestTreeNode("root")

    def test_get_subnodes(self, tree):
        # No builds -- subnodes() should return an empty collection.
        subnodes = tree.subnodes()
        assert isinstance(subnodes, nodes.NodeCollection)
        assert len(subnodes) == 0

    def test_get_subnodes_force_build(self, tree):
        # Two build calls should attach two subnodes in the test node.
        tree.build()
        tree.build()
        assert len(tree.subnodes()) == 2

    def test_get_subnodes_autobuild(self, tree):
        # Autobuilding subnodes() call should build once and thus have
        # one subnode.
        tree.autobuild = True
        subnodes = tree.subnodes()
        subnodes = tree.subnodes()
        assert len(subnodes) == 1

    def test_add_subnode(self, tree):
        new_node = nodes.Node("new_node")
        tree.add_subnode(new_node)
        assert new_node in tree.subnodes()

    def test_remove_subnode(self, tree):
        tree.build()
        tree.remove_subnode(self.sub1)
        assert self.sub1 not in tree.subnodes()

    def test_find_subnode(self, tree):
        tree.build()
        assert tree.find_subnode(self.sub1.identity) is self.sub1

    def test_find_subnode_fail(self, tree):
        # sub2 is only added on the second build.
        tree.build()
        assert tree.find_subnode(self.sub2.identity) is None

    def test_find_subnode_autobuild(self, tree):
        # Should build only once for first-time.
        tree.autobuild = True
        assert tree.find_subnode(self.sub1.identity) is self.sub1
        assert tree.find_subnode(self.sub2.identity) is None
        assert len(tree.subnodes()) == 1

    def test_find_subnode_autobuild_ondemand(self, tree):
        # Should build twice for first-time and on-demand.
        tree.autobuild = True
        assert tree.find_subnode(self.sub1.identity) is self.sub1
        tree.ondemand = True
        assert tree.find_subnode(self.sub2.identity) is self.sub2
        assert len(tree.subnodes()) == 2


@pytest.fixture
def tree():
    """Return a depth-first ordered dictionary containing the
    individual nodes that are connected to each other in a single tree::

        root
         +-node-1
         |  +-node-1-1
         |  +-node-1-2
         |     +-node-1-2-1
         +-node-2
         |  +-node-2-1
         +-node-3

    node-2-1 is a bare Node; all others are TreeNodes.
    """

    class _TestTreeNode(nodes.TreeNode):
        def build(self):
            self.built = True

    class _TestNode(nodes.Node):
        def build(self):
            self.built = True

    r = _TestTreeNode("root")
    n1 = _TestTreeNode("node-1")
    n11 = _TestTreeNode("node-1-1")
    n12 = _TestTreeNode("node-1-2")
    n121 = _TestTreeNode("node-1-2-1")
    n2 = _TestTreeNode("node-2")
    # node-2-1 is intentionally NOT a TreeNode.
    n21 = _TestNode("node-2-1")
    n3 = _TestTreeNode("node-3")

    n2.add_subnode(n21)
    n12.add_subnode(n121)
    n1.add_subnode(n11)
    n1.add_subnode(n12)
    r.add_subnode(n1)
    r.add_subnode(n2)
    r.add_subnode(n3)

    # Depth-first order in dictionary.
    tree_ = collections.OrderedDict()
    for n in [r, n1, n11, n12, n121, n2, n21, n3]:
        tree_[n.identity] = n
    return tree_


class TestTree(object):
    """Test :class:`lutopu.nodes.TreeNode` tree functionality with a
    populated tree.
    """

    def test_is_leaf(self, tree):
        # node-2-1 is not a TreeNode with an is_leaf() method.
        for identity in ['node-1-1', 'node-1-2-1', 'node-3']:
            assert tree[identity].is_leaf()

    def test_is_not_leaf(self, tree):
        for identity in ['root', 'node-1', 'node-1-2', 'node-2']:
            assert not tree[identity].is_leaf()

    def test_walk_order(self, tree):
        # walk() must return the correct depth-first order of nodes.
        # Note that node-2-1 is a non-iterable Node and walk() must
        # iterate through it correctly.
        root = tree['root']
        walk_count = 0
        for test_node, correct_node in zip(root.walk(), tree.values()):
            walk_count += 1
            assert test_node is correct_node
        assert len(tree) == walk_count

    def test_autobuilding_manager(self, tree):
        # Test autobuild context manager from the root. All nodes must
        # have the autobuild flag set.
        with tree['root'].autobuilding():
            for node in tree.values():
                assert node.autobuild and not node.ondemand
        # All flags must be cleared after context manager.
        for node in tree.values():
            assert not node.autobuild and not node.ondemand

    def test_autobuilding_manager_with_ondemand(self, tree):
        with tree['root'].autobuilding(ondemand=True):
            for node in tree.values():
                assert node.autobuild and node.ondemand
        for node in tree.values():
            assert not node.autobuild and not node.ondemand

    def test_autobuilding_manager_at_subtree(self, tree):
        # Only node-1 and its descendants should have the autobuild flag
        # set.
        with tree['node-1'].autobuilding():
            for ident, node in tree.items():
                if ident.startswith("node-1"):
                    assert node.autobuild
                else:
                    assert not node.autobuild
        for node in tree.values():
            assert not node.autobuild

    def test_autobuilding_manager_integrity(self, tree):
        # The autobuilding context manager MUST NOT build nodes in the
        # process of setting the nodes' flags.
        with tree['root'].autobuilding():
            for node in tree.values():
                assert not getattr(node, "built", False)


class TestPath(object):
    """Test :class:`lutopu.nav.Path`."""

    def test_instance(self):
        """Verify instantiation integrity."""

        assert str(nav.Path()) == "."
        assert str(nav.Path("")) == "."
        assert str(nav.Path("   ")) == "."
        assert str(nav.Path(" \t  \t")) == "."
        assert str(nav.Path("to/a/path")) == "to/a/path"
        assert str(nav.Path("to/a/path/")) == "to/a/path/"
        assert str(nav.Path("/to/a/path")) == "/to/a/path"
        assert str(nav.Path("/to/a/path/")) == "/to/a/path/"
        assert str(nav.Path("path")) == "path"
        assert str(nav.Path("path/")) == "path/"
        assert str(nav.Path("/path")) == "/path"
        assert str(nav.Path("/path/")) == "/path/"

    def test_affix_trailing_sep_return(self):
        path = nav.Path()
        affix_path = path.affix_trailing_sep()
        assert isinstance(affix_path, nav.Path)
        assert affix_path is not path

    def test_clear_trailing_sep_return(self):
        path = nav.Path()
        clear_path = path.clear_trailing_sep()
        assert isinstance(clear_path, nav.Path)
        assert clear_path is not path

    def test_normalize_return(self):
        path = nav.Path()
        normalize_path = path.normalize()
        assert isinstance(normalize_path, nav.Path)
        assert normalize_path is not path

    def test_split_return(self):
        path = nav.Path()
        head, tail = path.split()
        assert isinstance(tail, str)
        assert isinstance(head, nav.Path)
        assert head is not path

    def test_affix_trailing_seprator(self):
        assert (str(nav.Path("to/a/path").affix_trailing_sep()) ==
                "to/a/path/")
        assert (str(nav.Path("to/a/path/").affix_trailing_sep()) ==
                "to/a/path/")
        assert (str(nav.Path("to/a/path///").affix_trailing_sep()) ==
                "to/a/path///")
        assert str(nav.Path().affix_trailing_sep()) == "./"

    def test_clear_trailing_seprator(self):
        assert (str(nav.Path("to/a/path").clear_trailing_sep()) ==
                "to/a/path")
        assert (str(nav.Path("to/a/path/").clear_trailing_sep()) ==
                "to/a/path")
        assert (str(nav.Path("to/a/path///").clear_trailing_sep()) ==
                "to/a/path")
        assert str(nav.Path().clear_trailing_sep()) == "."

    def test_is_absolute(self):
        assert nav.Path("/").is_absolute()
        assert nav.Path("/file").is_absolute()
        assert nav.Path("/absolute/path").is_absolute()
        assert nav.Path("///absolute/path").is_absolute()
        assert not nav.Path("file").is_absolute()
        assert not nav.Path("non/absolute/path").is_absolute()

    def test_split_relative(self):
        head, tail = nav.Path("to/a/path").split()
        assert str(head) == "to/a" and tail == "path"
        head, tail = nav.Path("to/a/path/").split()
        assert str(head) == "to/a/path" and tail == ""

    def test_split_absolute(self):
        head, tail = nav.Path("/to/a/path").split()
        assert str(head) == "/to/a" and tail == "path"
        head, tail = nav.Path("/to/a/path/").split()
        assert str(head) == "/to/a/path" and tail == ""

    def test_split_single_segment_relative(self):
        head, tail = nav.Path("path").split()
        assert str(head) == "." and tail == "path"
        head, tail = nav.Path("path/").split()
        assert str(head) == "path" and tail == ""

    def test_split_single_segment_absolute(self):
        head, tail = nav.Path("/path").split()
        assert str(head) == "/" and tail == "path"
        head, tail = nav.Path("/path/").split()
        assert str(head) == "/path" and tail == ""

    def test_split_root(self):
        head, tail = nav.Path("/").split()
        assert str(head) == "/" and tail == ""
        head, tail = nav.Path("///").split()
        assert str(head) == "///" and tail == ""

    def test_normalize_root(self):
        assert str(nav.Path("/").normalize()) == "/"
        assert str(nav.Path("//").normalize()) == "/"
        assert str(nav.Path("///").normalize()) == "/"

    def test_normalize_empty(self):
        # The normalization of an empty path should return ".".
        assert str(nav.Path().normalize()) == "."
        assert str(nav.Path("./").normalize()) == "."
        assert str(nav.Path(".//").normalize()) == "."
        assert str(nav.Path(".///").normalize()) == "."

    # Most test paths are adapted from Python's POSIX normpath's test.

    def test_normalize_relative(self):
        assert str(nav.Path("foo/.//bar//").normalize()) == "foo/bar"
        assert (str(nav.Path("foo/.//bar//.//..//.//baz").normalize()) ==
            "foo/baz")

    def test_normalize_relative_outside_base(self):
        # Normalization of relative paths that trace beyond the known
        # highest directory should begin with successive "../" depending
        # on the depth.
        assert str(nav.Path("..//./foo/.//bar").normalize()) == "../foo/bar"
        assert str(nav.Path("foo/..//../bar//").normalize()) == "../bar"
        assert (str(nav.Path("../../foo/.//bar/..//baz").normalize()) ==
            "../../foo/baz")

    def test_normalize_absolute(self):
        # ".." markers that trace beyond the root should be dropped.
        assert str(nav.Path("///foo/.//bar//").normalize()) == "/foo/bar"
        assert (str(nav.Path("///foo/.//bar//.//..//.//baz").normalize()) ==
            "/foo/baz")
        assert str(nav.Path("///..//./foo/.//bar").normalize()) == "/foo/bar"

    def test_decompose(self):
        # Relative.
        seg, abs_flag = nav.Path("to/a/path").decompose()
        assert seg == ["to", "a", "path"] and not abs_flag
        seg, abs_flag = nav.Path("to/a/path/").decompose()
        assert seg == ["to", "a", "path"] and not abs_flag

        # Absolute.
        seg, abs_flag = nav.Path("/to/a/path").decompose()
        assert seg == ["to", "a", "path"] and abs_flag
        seg, abs_flag = nav.Path("/to/a/path/").decompose()
        assert seg == ["to", "a", "path"] and abs_flag

        # Multiple slashes.
        seg, abs_flag = nav.Path("to///a//path////").decompose()
        assert seg == ["to", "a", "path"] and not abs_flag
        seg, abs_flag = nav.Path("////to///a//path////").decompose()
        assert seg == ["to", "a", "path"] and abs_flag

        # Empty and root.
        seg, abs_flag = nav.Path("").decompose()
        assert seg == ["."] and not abs_flag
        seg, abs_flag = nav.Path("/").decompose()
        assert seg == [] and abs_flag
        seg, abs_flag = nav.Path("///").decompose()
        assert seg == [] and abs_flag


class TestPointer(object):
    """Test :class:`lutopu.nav.Pointer`."""

    def test_instance(self, tree):
        pointer = nav.Pointer(tree['root'])
        assert pointer.current is tree['root']

    def test_stay(self, tree):
        pointer = nav.Pointer(tree['root'])
        pointer.stay()
        assert pointer.current is tree['root']

    def test_advance(self, tree):
        pointer = nav.Pointer(tree['root'])
        pointer.advance("node-2")
        assert pointer.current is tree['node-2']
        pointer.advance("node-2-1")
        assert pointer.current is tree['node-2-1']

    def test_advance_predicate(self, tree):
        pointer = nav.Pointer(tree['root'])
        predicate = lambda n: isinstance(n, nodes.TreeNode)
        pointer.advance("node-2", predicate)
        assert pointer.current is tree['node-2']
        with pytest.raises(lutopu.exceptions.PointerError):
            pointer.advance("node-2-1", predicate)

    def test_advance_fail(self, tree):
        pointer = nav.Pointer(tree['root'])
        with pytest.raises(lutopu.exceptions.PointerError):
            pointer.advance("invalid-node")

        # If the pointer is on a non-TreeNode object, which does not
        # have a find_subnode() method, it must gracefully raise a
        # PointerError and not AttributeError when trying to advance.
        pointer = nav.Pointer(tree['node-2-1'])
        with pytest.raises(lutopu.exceptions.PointerError):
            pointer.advance("invalid-node")

    def test_back(self, tree):
        pointer = nav.Pointer(tree['node-2-1'])
        # back() on a base Node.
        pointer.back()
        assert pointer.current is tree['node-2']
        # back() on a TreeNode.
        pointer.back()
        assert pointer.current is tree['root']

    def test_back_from_root(self, tree):
        pointer = nav.Pointer(tree['root'])
        pointer.back()
        assert pointer.current is tree['root']

    def test_to_root(self, tree):
        pointer = nav.Pointer(tree['node-2-1'])
        pointer.to_root()
        assert pointer.current is tree['root']

    def test_select(self, tree):
        pointer = nav.Pointer(tree['root'])
        node = pointer.select(["node-2", "node-2-1"])
        assert node is pointer.current is tree['node-2-1']

    def test_select_peek(self, tree):
        pointer = nav.Pointer(tree['root'])
        node = pointer.select(["node-2", "node-2-1"], peek=True)
        assert node is tree['node-2-1'] and pointer.current is tree['root']

    def test_select_predicate(self, tree):
        pointer = nav.Pointer(tree['root'])
        predicate = lambda n: isinstance(n, nodes.TreeNode)
        with pytest.raises(lutopu.exceptions.PointerError):
            node = pointer.select(["node-2", "node-2-1"], predicate=predicate)

    def test_select_fail(self, tree):
        # When path selection fails, PointerError must be raised and the
        # pointer must revert to the node it had pointed to before
        # select() was called.
        pointer = nav.Pointer(tree['node-2'])
        predicate = lambda n: isinstance(n, nodes.TreeNode)

        # Advance error.
        with pytest.raises(lutopu.exceptions.PointerError):
            pointer.select(["invalid-node"])
        assert pointer.current is tree['node-2']

        # Predicate rejection.
        with pytest.raises(lutopu.exceptions.PointerError):
            pointer.select(["node-2-1"], predicate=predicate)
        assert pointer.current is tree['node-2']

        # Select from non-TreeNode.
        with pytest.raises(lutopu.exceptions.PointerError):
            pointer.select(["node-2-1", "invalid-node"])
        assert pointer.current is tree['node-2']

    def test_select_from_root(self, tree):
        pointer = nav.Pointer(tree['node-1'])
        node = pointer.select(["node-1", "node-1-2", "node-1-2-1"],
                              from_root=True)
        assert node is tree['node-1-2-1']

    def test_select_complex(self, tree):
        pointer = nav.Pointer(tree['node-1'])
        node = pointer.select([
            "node-1-2",
            "node-1-2-1",
            "..",
            "..",
            "..",
            "node-2",
            ".",
            "",
            "node-2-1",
            "",
            "",
            "."
        ])
        assert node is tree['node-2-1']

    def test_select_path(self, tree):
        pointer = nav.Pointer(tree['root'])
        predicate = lambda n: isinstance(n, nodes.TreeNode)

        node = pointer.select_path(nav.Path("node-1/../node-1/./node-1-1"))
        assert node is pointer.current is tree['node-1-1']

        node = pointer.select_path(nav.Path("/node-2/node-2-1"))
        assert node is pointer.current is tree['node-2-1']

        with pytest.raises(lutopu.exceptions.PointerError):
            pointer.select_path(nav.Path("/node-2/node-2-1"),
                                predicate=predicate)

        pointer.select_path(nav.Path("/node-3"))
        node = pointer.select_path(nav.Path("/node-2/node-2-1"), peek=True)
        assert node is tree['node-2-1'] and pointer.current is tree['node-3']

    def test_call(self, tree):
        pointer = nav.Pointer(tree['node-1-1'])
        assert pointer() is pointer.current
        assert isinstance(pointer(nav.Path()), nodes.Node)

        # Ensure that select_path() is called correctly.
        pointer.select_path = mock.MagicMock(name='select_path')
        predicate = lambda n: True
        path = nav.Path()
        pointer(path, peek=True, predicate=predicate)
        assert pointer.select_path.called
        assert pointer.select_path.call_args[0] == (path, True, predicate)
