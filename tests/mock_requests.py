"""Mock requests module that simulates put.io behavior for local unit
testing.
"""

import inspect
import json
import urllib.parse
from unittest import mock

import requests

from lutopu import net


ACCT_INFO_JSON = """{
    "info": {
        "username": "cenk",
        "mail": "cenk@gmail.com",
        "plan_expiration_date": "2014-03-04T06:33:30",
        "default_subtitle_language": "eng",
        "disk": {
            "avail": 20849243836,
            "used": 32837847364,
            "size": 53687091200
        }
    },
    "status": "OK"
}"""

# Raw JSON data for a hypothical parent directory id#123.
FILELIST_JSON = """{
    "files": [
        {
            "content_type": "text/plain",
            "crc32": "66a1512f",
            "created_at": "2013-09-07T21:32:03",
            "first_accessed_at": null,
            "icon": "https://put.io/images/file_types/text.png",
            "id": 6546533,
            "is_mp4_available": false,
            "is_shared": false,
            "name": "MyFile.txt",
            "opensubtitles_hash": null,
            "parent_id": 123,
            "screenshot": null,
            "size": 92
        },
        {
            "content_type": "video/x-matroska",
            "crc32": "cb97ba70",
            "created_at": "2013-09-07T21:32:03",
            "first_accessed_at": "2013-09-07T21:32:13",
            "icon": "https://put.io/thumbnails/aF5rkZVtYV9pV1iWimSOZWJjWWFaXGZdaZBmY2OJY4uJlV5pj5FiXg%3D%3D.jpg",
            "id": 7645645,
            "is_mp4_available": false,
            "is_shared": false,
            "name": "MyVideo.mkv",
            "opensubtitles_hash": "acc2785ffa573c69",
            "parent_id": 123,
            "screenshot": "https://put.io/screenshots/aF5rkZVtYV9pV1iWimSOZWJjWWFaXGZdaZBmY2OJY4uJlV5pj5FiXg%3D%3D.jpg",
            "size": 1155197659
        },
        {
            "content_type": "application/x-directory",
            "crc32": null,
            "created_at": "2013-09-07T21:32:03",
            "first_accessed_at": null,
            "icon": "https://put.io/images/file_types/folder.png",
            "id": 115932951,
            "is_mp4_available": false,
            "is_shared": false,
            "name": "Breaking.Bad.S05E09.720p.HDTV.x264-IMMERSE [PublicHD]",
            "opensubtitles_hash": null,
            "parent_id": 123,
            "screenshot": null,
            "size": 1155197751
        }
    ],
    "parent": {
        "content_type": "application/x-directory",
        "crc32": null,
        "created_at": "2013-09-07T21:32:03",
        "first_accessed_at": null,
        "icon": "https://put.io/images/file_types/folder.png",
        "id": 123,
        "is_mp4_available": false,
        "is_shared": false,
        "name": "MyFolder",
        "opensubtitles_hash": null,
        "parent_id": 0,
        "screenshot": null,
        "size": 1155197751
    },
    "status": "OK"
}"""

TRANSFERS_JSON = """{
    "status": "OK",
    "transfers": [
        {
            "availability": null,
            "callback_url": null,
            "created_at": "2012-03-28T09:14:17",
            "created_torrent": false,
            "current_ratio": 0.00,
            "down_speed": 1249337,
            "downloaded": 2999431,
            "error_message": null,
            "estimated_time": 5,
            "extract": false,
            "file_id": null,
            "finished_at": null,
            "id": 2293761,
            "is_private": false,
            "magneturi": "magnet:?xt=urn:btih:1234567890",
            "name": "A video",
            "peers_connected": 36,
            "peers_getting_from_us": 0,
            "peers_sending_to_us": 22,
            "percent_done": 30,
            "save_parent_id": 0,
            "seconds_seeding": 0,
            "size": 9409268,
            "source": "magnet:?xt=urn:btih:194a4c341487fd12d36718054c1e8fef4358b2ab3",
            "status": "DOWNLOADING",
            "status_message": "\u2193 1.2 MB/s, \u2191 0.0 bytes/s | connected to 36 peers, sending to 0 peers | dl: 2.9 MB / 9.0 MB, ul: 0.0 bytes",
            "subscription_id": null,
            "torrent_link": "/v2/transfers/123/torrent",
            "tracker_message": null,
            "trackers": [
                "udp://some.random.tracker:80/announce",
                "udp://another.tracker:4537"
            ],
            "tracker_message": null,
            "type": "TORRENT",
            "up_speed": 0,
            "uploaded": 0
        }
    ]
}"""


class MockRequests(object):
    """Shallow simulation of the :mod:`requests` library for put.io-
    specific functionality.
    """

    access_token = "54321"
    """The ``oauth_token`` parameter for requests must be this value or
    else an "unauthorized" error is returned.
    """

    user_name = "max@some.domain"
    password = "p4ssw0rd"
    """Test user credentials."""

    def __init__(self):
        self.data_to_use = self.get_full_data()
        self.random_dirs = []

    def get_full_data(self):
        """Return entire data tree as a dictionary."""
        return json.loads(FILELIST_JSON)

    def get_filelist_data(self):
        """Return just the file list, preprocessed by the
        :mod:`lutopu.net` module as it would be retrieved by its fetch
        functions. Each element can be used as the argument for the
        :class:`~lutopu.nodes.putio.FetchData` constructor.
        """
        return net._FetchFileList(None).process_data(self.get_full_data())

    def get_parent_data(self):
        """Return just the parent directory data. It can be used as the
        argument for the :class:`~lutopu.nodes.putio.DirectoryNode`
        constructor.
        """
        return self.get_full_data()['parent']

    def data_iter(self):
        """Iterate the items of the parent directory, including itself.
        """
        for item in self.data_to_use['files'] + [self.data_to_use['parent']]:
            yield item

    def get(self, url, **kwargs):
        """Simulate :func:`requests.get()`."""
        return self._do_response(url, kwargs.get("params", {}))

    def post(self, url, **kwargs):
        """Simulate :func:`requests.post()`."""
        return self._do_response(url, kwargs.get("data", {}))

    def _do_response(self, url, payload):
        """Route the endpoint API URL and payload to the appropriate
        handler method and return the results as a mock
        :class:`requests.Response` object.
        """

        response = mock.MagicMock(
            spec=requests.Response,
            url=url
        )

        if url == net.LOGIN_URL:
            func_target_name = "_handle_login"
        else:
            # Check oauth access token
            if payload.get("oauth_token", None) != self.access_token:
                response.status_code, response.text = _RequestError.unauthorized
            endpoint = url[len(net.API_URL):]
            endpoint = endpoint.replace("/", "_").replace("-", "_")
            func_target_name = "_handle_{}".format(endpoint)

        # Match requested endpoint to handler function.
        for name, func in inspect.getmembers(self,
                                             predicate=inspect.ismethod):
            if name == func_target_name:
                try:
                    response.text = func(payload, url, response)
                    response.status_code = 200
                except _RequestError as err:
                    response.text = err.text
                    response.status_code = err.status_code
                break
        else:
            response.status_code, response.text = _RequestError.not_found

        response.json.return_value = json.loads(response.text)
        return response

    def _handle_login(self, payload, url, response):
        valid = False
        try:
            uri_path = urllib.parse.urlparse(url)[2]
            user_name = payload['name']
            password = payload['password']
            next_uri = payload['next']

            next_uri_parts = urllib.parse.urlparse(next_uri)
            next_uri_path = next_uri_parts[2]
            next_uri_query = next_uri_parts[4]

            parsed_nqs = urllib.parse.parse_qs(next_uri_query)

            if (uri_path == "/login" and
                    next_uri_path == "/v2/oauth2/authenticate"):
                valid = all((parsed_nqs['response_type'] == ['token'],
                             parsed_nqs['redirect_uri'] == [net.REDIRECT_URI],
                             parsed_nqs['client_id'] == [str(net.CLIENT_ID)],
                             user_name == self.user_name,
                             password == self.password))
        except KeyError:
            pass
        if valid:
            response.url = "{}#access_token={}".format(net.REDIRECT_URI,
                                                       self.access_token)
            return "{}"
        raise _RequestError.new.not_found

    def _handle_account_info(self, payload, url, response):
        return ACCT_INFO_JSON

    def _handle_files_list(self, payload, url, response):
        parent_id = verify_params(payload, ['parent_id'])[0]

        for f in self.data_iter():
            if parent_id == f['id']:
                # Can't take list of non-directories.
                if f['content_type'] != "application/x-directory":
                    raise _RequestError.new.invalid_parent
                return json.dumps(self.data_to_use)
        raise _RequestError.new.not_found

    def _handle_files_create_folder(self, payload, url, response):
        parent_id, name = verify_params(payload, ['parent_id', 'name'])

        # Yes, put.io allows you to create folders on files, which
        # seems to do nothing. No, I have no idea why it does.
        for f in self.data_iter():
            if parent_id == f['id']:
                return """{{
                    "file": {{
                        "content_type": "application/x-directory",
                        "crc32": null,
                        "created_at": "2013-09-07T21:32:03",
                        "first_accessed_at": null,
                        "icon": "https://put.io/images/file_types/folder.png",
                        "id": 12345,
                        "is_mp4_available": false,
                        "is_shared": false,
                        "name": "{folder_name}",
                        "opensubtitles_hash": null,
                        "parent_id": 123,
                        "screenshot": null,
                        "size": 1155197751
                    }},
                    "status": "OK"
                }}""".format(folder_name=name)
        raise _RequestError.new.not_found

    def _handle_files_delete(self, payload, url, response):
        file_ids = verify_params(payload, ['file_ids'])[0]

        # Cannot delete 123 base directory right now.
        new_files = []
        delete_flag = False
        for f in self.data_to_use['files']:
            if f['id'] in file_ids:
                delete_flag = True
            else:
                new_files.append(f)

        if not delete_flag:
            raise _RequestError.new.not_found

        self.data_to_use['files'] = new_files
        return """{"status": "OK"}"""

    def _handle_files_move(self, payload, url, response):
        file_ids, parent_id = verify_params(payload, ['file_ids', 'parent_id'])

        # Cannot move 123 base directory right now.
        new_files = []
        move_flag = False
        for f in self.data_to_use['files']:
            if f['id'] in file_ids:
                move_flag = True
            else:
                new_files.append(f)

        if not move_flag:
            raise _RequestError.new.not_found
        if not parent_id in [115932951]:
            raise _RequestError.new.invalid_parent

        self.data_to_use['files'] = new_files
        return """{"status": "OK"}"""

    def _handle_files_rename(self, payload, url, response):
        file_id, name = verify_params(payload, ['file_id', 'name'])
        for f in self.data_iter():
            if f['id'] == file_id:
                f['name'] = name
                return """{"status": "OK"}"""
        raise _RequestError.new.not_found

    def _handle_transfers_list(self, payload, url, response):
        return TRANSFERS_JSON


def verify_params(kwargs, spec):
    """Verify that *kwargs* contains the keys specified by the *spec*
    list and do some keyname-sensitive validation and modification
    to the values. Raises :exc:`_RequestError` if a required argument
    does not exist or fails validation.

    Returns a list of the arguments in the order of *spec*.
    """

    params = []
    for name in spec:
        if name not in kwargs or not kwargs[name]:
            raise _RequestError.new.insufficient_params
        value = kwargs[name]
        if name == "name" and value.find("\\") != -1:
            raise _RequestError.new.invalid_name
        # file_ids input can be a number or a comma-delimited string.
        if name == "file_ids":
            try:
                value = [int(v) for v in value.split(',')]
            except AttributeError:
                value = [value]
        params.append(value)
    return params


class _NewRequestError(object):
    """Construction descriptor that creates new :exc:`_RequestError`
    instances by accessing its attributes.
    """

    def __init__(self):
        self.parent_class = None

    def __get__(self, instance, owner):
        self.parent_class = owner
        return self

    def __getattr__(self, key):
        try:
            error_tuple = getattr(self.parent_class, key)
        except AttributeError:
            raise ValueError("'{}' is not a valid {} error".format(
                key, self.parent_class.__name__))
        return self.parent_class(error_tuple)


class _RequestError(Exception):
    """Used by :class:`MockRequest` handlers to raise simulated server
    errors. Use the :attr:`_RequestError.new` factory constructor
    to create new instances of a pre-defined error type, e.g.::

        >>> _RequestError.new.unauthorized
        >>> _RequestError.new.not_found
    """
    unauthorized = (401, """{
        "error_message": "The server could not verify that you are authorized to access the URL requested.  You either supplied the wrong credentials (e.g. a bad password), or your browser doesn't understand how to supply the credentials required.",
        "error_type": "Unauthorized",
        "error_uri": "http://api.put.io/v2/docs",
        "status": "ERROR",
        "status_code": 401
    }""")

    not_found = (404, """{
        "error_message": "The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.",
        "error_type": "NotFound",
        "error_uri": "http://api.put.io/v2/docs",
        "status": "ERROR",
        "status_code": 404
    }""")

    invalid_parent = (400, """{
        "error_message": "Parent is not a folder",
        "error_type": "BadRequest",
        "error_uri": "http://api.put.io/v2/docs",
        "status": "ERROR",
        "status_code": 400
    }""")

    invalid_name = (400, """{
        "error_message": "Name cannot contain \\"/\\"",
        "error_type": "BadRequest",
        "error_uri": "http://api.put.io/v2/docs",
        "status": "ERROR",
        "status_code": 400
    }""")

    insufficient_params = (400, """{
        "error_message": "The browser (or proxy) sent a request that this server could not understand.",
        "error_type": "BadRequest",
        "error_uri": "http://api.put.io/v2/docs",
        "status": "ERROR",
        "status_code": 400
    }""")

    new = _NewRequestError()

    def __init__(self, error):
        super().__init__()
        self.status_code, self.text = error

