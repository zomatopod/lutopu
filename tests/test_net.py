import threading
import queue
import contextlib
import functools
import time
import urllib.parse

try:
    from unittest import mock
except ImportError:
    import mock

import pytest
import requests

from lutopu import net
import lutopu.exceptions


@contextlib.contextmanager
def block():
    """Helper to block until specified callback functions have been
    called::

        import threading
        import queue
        import time

        main_queue = queue.Queue()

        def do_something():
            callback = main_queue.get()
            time.sleep(5)
            callback()

        threading.Thread(target=do_something).start()

        with block() as blocker:
            foo = None

            @blocker.waitfor
            def callback_func():
                global foo
                foo = 55

            main_queue.put(callback_func)
            blocker.wait()
            assert foo == 55
    """

    blocker = queue.Queue()
    waitfor_funcs = []

    def waitfor(func):
        waitfor_funcs.append(func)
        @functools.wraps(func)
        def wrap(*args, **kwargs):
            retv = func(*args, **kwargs)
            blocker.put(func)
            return retv
        return wrap

    def wait(timeout=1):
        waiting = list(waitfor_funcs)
        for _ in range(len(waiting)):
            try:
                finished_func = blocker.get(timeout=timeout)
                waiting.remove(finished_func)
            except queue.Empty:
                raise Exception(
                    "Block timed-out waiting for: {}".format(waiting)
                )

    blocker.waitfor = waitfor
    blocker.wait = wait

    try:
        yield blocker
    finally:
        del blocker.waitfor
        del blocker.wait


def test_login(mock_requests):
    with mock.patch("requests.post", mock_requests.post):
        net.access_token = None
        net.login("max@some.domain", "p4ssw0rd")
        assert net.access_token == mock_requests.access_token


def test_login_fail(mock_requests):
    with mock.patch("requests.post", mock_requests.post):
    # Bad credentials.
        with pytest.raises(lutopu.exceptions.UnauthorizedError):
            net.login("max@some.domain", "not_right")
    # Server error.
    with mock.patch("requests.post", mock.MagicMock()) as raise_exc:
        raise_exc.side_effect = requests.ConnectionError()
        with pytest.raises(lutopu.exceptions.ServerConnectionError):
            net.login("max@some.domain", "p4ssw0rd")


def test_login_callback(mock_requests):
    with block() as blocker:
        error = None

        @blocker.waitfor
        def callback(d,e):
            nonlocal error
            error = e

        # Test success.
        with mock.patch("requests.post", mock_requests.post):
            net.access_token = None
            net.login("max@some.domain", "p4ssw0rd", callback)
            blocker.wait()
            assert net.access_token == mock_requests.access_token
            assert error is None
        # Test failure.
        with mock.patch("requests.post", mock.MagicMock()) as raise_exc:
            raise_exc.side_effect = requests.ConnectionError()
            net.login("max@some.domain", "p4ssw0rd", callback)
            blocker.wait()
            assert isinstance(error, lutopu.exceptions.ServerConnectionError)


def test_clear_access_token():
    net.access_token = 12345
    net.clear_access_token()
    assert net.access_token is None


def test_nettask_instance():
    callback = lambda d,e: None
    proc_func = lambda d: None
    payload = {}
    task = net.NetTask("name", "http://some.url", net.NetTaskRequestType.get,
                       callback, payload, proc_func)
    assert task.name == "name"
    assert task.url == "http://some.url"
    assert task.request_type == net.NetTaskRequestType.get
    assert task.callback is callback
    assert task.request_data is payload
    assert task.proc_func is proc_func

    # Default parameters.
    task = net.NetTask("name", "http://some.url")
    assert task.request_type == net.NetTaskRequestType.get
    assert task.callback is None
    assert task.request_data is None
    assert callable(task.proc_func)


class TestNetTaskDispatcher(object):
    queue = None

    @pytest.fixture
    def dispatcher(self, request):
        self.queue = queue.Queue()
        ntd = net.NetTaskDispatcher(self.queue)
        ntd.start()
        request.addfinalizer(ntd.stop.set)
        return ntd

    @pytest.fixture
    def task(self):
        return net.NetTask(
            name="Task",
            url="http://test.domain/endpoint",
            request_type=net.NetTaskRequestType.get,
            request_data={"param1": 10, "param2": "string"},
            proc_func=None,
            callback=lambda d,e: None
        )

    @pytest.fixture
    def response_ok(self):
        # Mock success response with json() method.
        resp = mock.MagicMock()
        resp.json.return_value = {'status': "OK", 'value': 12345}
        return resp

    @pytest.fixture
    def response_error(self):
        # Mock error response with json() method.
        resp = mock.MagicMock()
        resp.json.return_value = {
            "error_message": "A bad request error.",
            "error_type": "BadRequest",
            "error_uri": "http://api.put.io/v2/docs",
            "status": "ERROR",
            "status_code": 400
        }
        return resp

    @pytest.fixture
    def response_unauth(self):
        resp = mock.MagicMock()
        resp.json.return_value = {
            "error_message": "Unauthorized error.",
            "error_type": "Unauthorized",
            "error_uri": "http://api.put.io/v2/docs",
            "status": "ERROR",
            "status_code": 401
        }
        return resp

    def test_instance(self, dispatcher):
        assert dispatcher.queue == self.queue
        assert dispatcher.daemon == True

    def test_stop(self, dispatcher):
        assert dispatcher.is_alive()
        dispatcher.stop.set()
        time.sleep(0.25)
        assert not dispatcher.is_alive()

    @mock.patch("requests.get")
    @mock.patch("requests.post")
    def test_call_requests(self, mock_post, mock_get, response_ok,
                           dispatcher, task):
        # Test that requests get() and post() are called correctly.

        mock_get.return_value = response_ok
        mock_post.return_value = response_ok

        headers = {
            "Accept": "application/json"
        }

        # Test get request.
        with block() as blocker:
            @blocker.waitfor
            def callback(d, e):
                pass
            task.callback = callback
            task.request_type = net.NetTaskRequestType.get
            self.queue.put(task)
            blocker.wait()

            assert mock_get.call_args[0] == (task.url,)
            assert mock_get.call_args[1].get("headers", None) == headers
            assert (mock_get.call_args[1].get("params", None) is
                    task.request_data)

        # Test post request.
        with block() as blocker:
            @blocker.waitfor
            def callback(d, e):
                pass
            task.callback = callback
            task.request_type = net.NetTaskRequestType.post
            self.queue.put(task)
            blocker.wait()

            assert mock_post.call_args[0] == (task.url,)
            assert mock_post.call_args[1].get("headers", None) == headers
            assert (mock_post.call_args[1].get("data", None) is
                    task.request_data)

    @mock.patch("requests.get")
    def test_data_return(self, mock_get, response_ok, dispatcher, task):
        # Test that correct data is passed to the callback.
        mock_get.return_value = response_ok

        data = None
        error = None

        with block() as blocker:
            @blocker.waitfor
            def callback(d, e):
                nonlocal data, error
                data, error = d, e

            task.callback = callback
            self.queue.put(task)
            blocker.wait()
            assert data == response_ok.json.return_value
            assert error is None

    @mock.patch("requests.get")
    def test_error_return(self, mock_get, response_error, response_unauth,
                          dispatcher, task):
        # Test that correct error exception is passed to the callback.
        # UnauthorizedError must be passed for access token failures, and
        # RequestErrors for everything else.

        data = None
        error = None

        with block() as blocker:
            @blocker.waitfor
            def callback(d, e):
                nonlocal data, error
                data, error = d, e

            task.callback = callback
            mock_get.return_value = response_error
            self.queue.put(task)
            blocker.wait()
            assert data is None
            assert isinstance(error, lutopu.exceptions.RequestError)

            mock_get.return_value = response_unauth
            self.queue.put(task)
            blocker.wait()
            assert data is None
            assert isinstance(error, lutopu.exceptions.UnauthorizedError)

    @mock.patch("requests.get")
    def test_proc_func(self, mock_get, response_ok, dispatcher, task):
        # Test that the post-process function is invoked properly.
        mock_get.return_value = response_ok

        data = None

        with block() as blocker:
            @blocker.waitfor
            def callback(d, e):
                nonlocal data
                data = d

            @blocker.waitfor
            def process(d):
                return d['value'] + 256

            task.callback = callback
            task.proc_func = process
            self.queue.put(task)
            blocker.wait()
            assert data == response_ok.json.return_value['value'] + 256

    @mock.patch("requests.get")
    def test_retries(self, mock_get, response_error, dispatcher, task):
        # Test that connection failures are retried and raises the
        # appropriate errors once retries have been exhausted.
        mock_get.side_effect = requests.ConnectionError()

        error = None

        with block() as blocker:
            @blocker.waitfor
            def callback(d, e):
                nonlocal error
                error = e

            task.callback = callback
            self.queue.put(task)
            blocker.wait()

            assert mock_get.call_count == net.NET_RETRIES
            assert isinstance(error, lutopu.exceptions.ServerConnectionError)


class TestNetTasker(object):
    queue = None

    @pytest.fixture
    def tasker(self):
        class _TestNetTasker(net.NetTasker):
            name = "Test Task"
            endpoint = "test/url"
            request_type = net.NetTaskRequestType.post

        self.queue = queue.Queue()
        return _TestNetTasker(self.queue)

    def test_instance(self, tasker):
        assert tasker.queue is self.queue

    def test_process_data_default(self, tasker):
        test_data = {'status': "OK"}
        assert tasker.process_data(test_data) == "OK"

    def test_send_task(self, tasker):
        request_data = {"param1": 1, "param2": "test data"}
        do_error = False
        task = None

        def process_task_thread():
            # Do not assert or otherwise terminate from another thread.
            nonlocal task
            task = self.queue.get()
            if do_error:
                task.callback(None, TypeError())
            else:
                task.callback(12345, None)

        # Task success.
        threading.Thread(target=process_task_thread).start()
        assert tasker.send_task(request_data) == 12345
        assert task.request_type == net.NetTaskRequestType.post
        assert task.request_data == request_data
        assert task.proc_func == tasker.process_data
        assert task.name == "Test Task"
        assert task.url == urllib.parse.urljoin(net.API_URL, "test/url")

        # Task error.
        do_error = True
        threading.Thread(target=process_task_thread).start()
        with pytest.raises(TypeError):
            tasker.send_task(request_data)

    def test_send_task_callback(self, tasker):
        do_error = False
        data = None
        error = None

        def process_task_thread():
            task = self.queue.get()
            if do_error:
                task.callback(None, TypeError())
            else:
                task.callback(12345, None)

        with block() as blocker:
            @blocker.waitfor
            def callback(d, e):
                nonlocal data, error
                data, error = d, e

            # Task success.
            threading.Thread(target=process_task_thread).start()
            tasker.send_task({}, callback)
            blocker.wait()
            assert data == 12345 and error is None

            # Task error.
            do_error = True
            threading.Thread(target=process_task_thread).start()
            tasker.send_task({}, callback)
            blocker.wait()
            assert data is None and isinstance(error, TypeError)


def test_fetch_acct_info(mock_requests):
    with mock.patch("requests.get", mock_requests.get):
        acct_info = net.fetch_acct_info()
        assert acct_info['username'] == "cenk"
        assert acct_info['mail'] == "cenk@gmail.com"


def test_fetch_transfers(mock_requests):
    with mock.patch("requests.get", mock_requests.get):
        transfers = net.fetch_transfers()
        assert len(transfers) == 1
        assert transfers[0]['id'] == 2293761
        # Ensure that the data is sent to processing function before
        # being returned -- time_seeding_str is not a standard put.io
        # datum.
        assert "time_seeding_str" in transfers[0]


def test_fetch_file_list(mock_requests):
    with mock.patch("requests.get", mock_requests.get):
        file_list = net.fetch_file_list(123)
        assert len(file_list) == 3
        assert file_list[0]['id'] == 6546533
        assert file_list[1]['id'] == 7645645
        assert file_list[2]['id'] == 115932951
        # size_str is not a standard put.io datum.
        assert "size_str" in file_list[2]


def test_send_create_dir(mock_requests):
    with mock.patch("requests.post", mock_requests.post):
        created_dir = net.send_create_dir("NewDir", 123)
        assert created_dir['id'] == 12345
        assert created_dir['name'] == "NewDir"


def test_send_delete(mock_requests):
    with mock.patch("requests.post", mock_requests.post):
        response = net.send_delete(7645645)
        assert len(mock_requests.data_to_use['files']) == 2
        assert response == "OK"


def test_send_move(mock_requests):
    with mock.patch("requests.post", mock_requests.post):
        response = net.send_move(6546533, 115932951)
        # MockRequests currently does not perform any testable actions
        # for move.
        assert response == "OK"


def test_send_rename(mock_requests):
    with mock.patch("requests.post", mock_requests.post):
        response = net.send_rename(115932951, "New Name")
        assert mock_requests.data_to_use['files'][2]['name'] == "New Name"
        assert response == "OK"
