from lutopu import net
from lutopu.nodes.base import Node, TreeNode
from lutopu.exceptions import BuildError, RequestError


class PutioRoot(TreeNode):
    """The root of a put.io navigation tree. When activated, the node
    contains a branch for user files and one for transfers.
    """

    def __init__(self):
        super().__init__("root")
        self._activated = False

    @property
    def activated(self):
        return self._activated

    @activated.setter
    def activated(self, value):
        if value == True and self._activated == False:
            self._subnodes.append(DirectoryNode())
            self._subnodes.append(TransfersNode())
        elif value == False and self._activated == True:
            self._subnodes.clear()

        self._activated = value

    def reset(self):
        """Regenerates the root back to a virgin state."""

        if self.activated:
            self.activated = False
            self.activated = True


class FetchData(Node):
    def __init__(self, data):
        super().__init__(data['name'])
        self.id = data['id']
        self.data_type = data['data_type']
        self.data = data


# TODO: RequestErrors should remove nodes from the branch in all remote
# operations, not just delete.
class DirectoryNode(TreeNode):
    """Represents a directory in put.io's user files. Each
    :class:`DirectoryNode` may contain files in its :attr:`contents`
    and further subdirectories as :attr:`subnodes`. Like all put.io
    content, directories have an associated unique, numeric identifier,
    and the root of all files is ``0``.
    """

    build_timeout_secs = 60

    def __init__(self, data=None):
        if data is None:
            super().__init__("files")
            self.id = 0
        else:
            super().__init__(data['name'])
            self.id = data['id']

        self.data = data

        # Lookup of this node's subnodes, where the keys are their
        # put.io IDs and the values their indices in _subnodes. This is
        # populated in build() and rebuild_lookup().
        self._subnodes_lookup = {}

    def build(self):
        # Grab the file listing for the directory this node represents
        # from put.io. Subdirectories are represented as DirectoryNode
        # subnodes and all other files as the node's contents.

        try:
            listing = net.fetch_file_list(self.id)
        except RequestError:
            raise BuildError(
                "Directory `{}` (id:{}) is invalid".format(
                    self.identity, self.id
                )
            )

        nodes_to_add = []

        for data in listing:
            # Check to see if have a subnode representing this item
            # already. If we do, then we'll use the one we already have,
            # or else create one.
            try:
                idx = self._subnodes_lookup[data['id']]
                node = self._subnodes[idx]
                # Renamed files/subdirs will need a new node.
                # NOTE: Do not use the node's identity to compare this
                # since it is normalized.
                if node.data['name'] != data['name']:
                    raise KeyError
            except KeyError:
                if data['content_type'] == "application/x-directory":
                    node = DirectoryNode(data)
                else:
                    node = FetchData(data)

            nodes_to_add.append(node)

        # Apply the nodes as child subnodes and build the lookup
        # dictionary.

        self._subnodes.clear()
        self._subnodes_lookup = {}

        for idx, node in enumerate(nodes_to_add):
            self._subnodes_lookup[node.id] = idx
            self._subnodes.append(node)

    def rebuild_lookup(self):
        self._subnodes_lookup = {}
        for idx, node in enumerate(self._subnodes):
            self._subnodes_lookup[node.id] = idx

    def add_subnode(self, node):
        super().add_subnode(node)
        self.rebuild_lookup()

    def remove_subnode(self, node):
        super().remove_subnode(node)
        self.rebuild_lookup()

    def create_directory(self, name):
        """Create a new subdirectory of *name* remotely on put.io under
        this directory object. A new local subnode will also be
        generated for :attr:`subnodes` without rebuilding.

        Raises :exc:`ValueError` if put.io rejects the directory name.
        """

        new_item = net.send_create_dir(name, self.id)
        new_node = DirectoryNode(new_item)
        self.add_subnode(new_node)

    def delete(self, node):
        """Remotely delete the directory or file represented by *node*
        on put.io, and removes the node from :attr:`subnodes` without
        rebuilding. The node must be a child of this directory object,
        or else :exc:`ValueError` will be raised.

        :exc:`ValueError` is also raised if the specified node no longer
        exists on put.io. It is still removed from the local directory
        object.
        """

        if not node in self._subnodes:
            raise ValueError(
                "`{}` is not a file or subdirectory of {}".format(
                    node.identity,
                    self.identity
                )
            )

        try:
            net.send_delete(node.id)
        finally:
            self.remove_subnode(node)

    def move(self, source, target):
        """Move a *source* directory or file on put.io to the *target*
        directory node.
        """

        if not source in self._subnodes:
            raise ValueError(
                "`{}` is not a file or subdirectory of {}".format(
                    source.identity,
                    self.identity
                )
            )

        net.send_move(source.id, target.id)
        self.remove_subnode(source)
        target.add_subnode(source)

    def rename(self, node, name):
        if not node in self._subnodes:
            raise ValueError(
                "`{}` is not a file or subdirectory of {}".format(
                    node.identity,
                    self.identity
                )
            )

        net.send_rename(node.id, name)
        node.identity = name
        node.data['name'] = name

    def __repr__(self):
        return "<DirectoryNode:{}, {}>".format(self.identity, self.id)


class TransfersNode(TreeNode):
    """Represents put.io transfers. All transfers that have not been
    cleared off the transfers list are kept in :attr:`contents`. It
    does not keep any subnodes.
    """

    def __init__(self):
        super().__init__("transfers")

    def build(self):
        self._subnodes.clear()

        for transfer in net.fetch_transfers():
            self._subnodes.append(FetchData(transfer))
