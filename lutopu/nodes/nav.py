from lutopu.nodes.base import SEPARATOR, CURRENT, PARENT
from lutopu.exceptions import PointerError


class Path(object):
    def __init__(self, path=""):
        path = path.strip()
        self.path_str = path if path else CURRENT

    def affix_trailing_sep(self):
        """Return a new path with a trailing path separator."""

        if not self.path_str.endswith(SEPARATOR):
            return Path(self.path_str + SEPARATOR)
        return Path(self.path_str)

    def clear_trailing_sep(self):
        """Return a new path without a trailing path separator."""

        return Path(self.path_str.rstrip(SEPARATOR))

    def normalize(self):
        """Return a normalized path in a facsimile of POSIX rules."""

        absolute = self.is_absolute()
        segments = []

        for test_seg in self.path_str.split(SEPARATOR):
            if test_seg == "." or not test_seg:
                continue
            elif test_seg == "..":
                # Remove the last segment.
                #
                # If there are no more segments to remove, the ".." will
                # be added as-is in the else clause. When this happens,
                # successive ".." segments (i.e. "../../") will be added
                # in else, as well.
                if segments and segments[-1] != "..":
                    segments.pop()
                # The root has no parent, so drop this ".." instead of
                # adding it.
                elif not segments and absolute:
                    continue
                else:
                    segments.append(test_seg)
            else:
                segments.append(test_seg)

        path = SEPARATOR.join(segments)
        if absolute:
            path = SEPARATOR + path
        if path == "":
            path = "."
        return Path(path)

    def is_absolute(self):
        return self.path_str.startswith(SEPARATOR)

    def split(self):
        """Split the path into head and tail components, where head is
        the components up to the final separator, and tail is the string
        after it, useful for separating a directory + filename pair.

        Returns a ``(head, tail)`` tuple, where head is a new
        :class:`Path` and tail is a string.
        """

        i = self.path_str.rfind(SEPARATOR) + 1
        head, tail = self.path_str[:i], self.path_str[i:]
        # Strip trailing sep from head if it isn't the root, e.g. "/".
        if head != SEPARATOR*len(head):
            head = head.rstrip(SEPARATOR)
        return Path(head), tail

    def decompose(self):
        """Decompose the path into a list of its components. Returns a
        a tuple of a list of path segemnts and a boolean of whether or
        not the path is absolute.
        """

        return ([s for s in self.path_str.split(SEPARATOR) if s],
                self.is_absolute())

    def __repr__(self):
        return self.path_str


class Pointer(object):
    """Linear navigation of a tree of :class:`TreeNode` objects."""

    def __init__(self, current_node):
        self.current = current_node

    def stay(self):
        pass

    def advance(self, identity, predicate=None):
        predicate = predicate or (lambda node: True)
        try:
            node = self.current.find_subnode(identity)
        except AttributeError:
            pass
        else:
            if node and predicate(node):
                self.current = node
                return
        raise PointerError("Could not find `{}`".format(identity))

    def back(self):
        if self.current.parent:
            self.current = self.current.parent

    def to_root(self):
        self.current = self.current.root

    def select(self, selector, from_root=False, peek=False, predicate=None):
        """Select a node from a tree given a list of identities. Empty
        names are ignored.

        Explictly raises :exc:`PointerError` if a node could be
        selected.
        """

        # For restoration on selection failure or peek mode.
        old_node = self.current

        try:
            if from_root:
                self.to_root()
            for segment in selector:
                if segment == CURRENT:
                    self.stay()
                elif segment == PARENT:
                    self.back()
                elif segment:
                    self.advance(segment, predicate)
                else:
                    pass
        except PointerError:
            self.current = old_node
            raise

        selected_node = self.current
        if peek:
            self.current = old_node

        return selected_node

    def select_path(self, path, peek=False, predicate=None):
        """Select a node given a :class:`Path`. Returns and raises the
        same as :meth:`select`.
        """

        node = self.select(*path.decompose(), peek=peek, predicate=predicate)
        return node

    def __call__(self, path=None, peek=False, predicate=None):
        """Return the current node if *selector* is not specified,
        otherwise selects a node. Returns and raises the same as
        :meth:`select` and :meth:`select_path`.
        """

        if path is not None:
            return self.select_path(path, peek, predicate)
        else:
            return self.current
