import time
import weakref
import contextlib
from functools import wraps


#: Delimiting path character.
SEPARATOR = "/"

CURRENT = "."
PARENT = ".."


# TODO: Spaces in filenames are converted to underscores for parsing
# and user entry purposes. Right now it doesn't check if there already
# is a filename with that same name.

# TODO: Relatedly, put.io totally allows duplicate filenames since it
# tracks by uid, not names. There needs to be a way for the user to
# disambiguate these situations.

# TODO: Also related to this stupid nonsense is that files/directories
# with the same name will appear as one entry when autocompleting.



###############################################################################
# AUTOBUILDER
###############################################################################

class AutobuildMeta(type):
    """Metaclass to setup autobuilding mechanics on an object.

    An autobuilding object uses two decorators to define building
    methods: :func:`builder`, which defines the method to call when the
    object needs to build, and :func:`autobuild`, which are methods that
    can trigger autobuilding. The class *must* define a builder; an
    exception will be raised if it does not.

    The machinery that performs the actual autobuilding is
    :class:`Autobuilder`. The metaclass applies :class:`Autobuilder`
    descriptors and proxy functions to the decorated class attributes.
    """

    def __new__(mcs, name, bases, namespace):
        # Find the class's builder -- decorated with @builder -- and
        # wrap it with the Autobuilder build proxy. "_builder_func_name"
        # is set as a class attribute, which is used by both the
        # metaclass and Autobuilder to identify and resolve the builder
        # through the inheritance chain.

        # If the class explicitly defines a @builder method.
        for aname, value in namespace.items():
            if hasattr(value, "_ab_builder") and callable(value):
                namespace['_builder_func_name'] = aname
                namespace[aname] = Autobuilder.get_builder(value)
                break
        # It didn't, so try to get one from its parent classes.
        else:
            for base in bases:
                try:
                    builder_func_name = base._builder_func_name
                except AttributeError:
                    continue
                # If this is true, then the class has an attribute that
                # overrides a @builder from an ancestor class but isn't
                # explicitly decorated with @builder itself. We need to
                # wrap it with a proxy.
                if builder_func_name in namespace:
                    namespace[builder_func_name] = Autobuilder.get_builder(
                        namespace[builder_func_name])
                break
            else:
                raise Exception("{} has no @builder object".format(name))

        # Replace all @autobuild decorated callables with the
        # Autobuilder descriptor, which does the actual magic.
        for aname, value in namespace.items():
            if hasattr(value, "_ab_autobuild") and callable(value):
                namespace[aname] = Autobuilder(value, builder_func_name)

        return super().__new__(mcs, name, bases, namespace)


class Autobuilder(object):
    """Descriptor that ensures an object has been built before a method
    is called. Autobuilder is applied to a class via
    :class:`AutobuildMeta` and should not be used directly.

    Autobuilds are initiated for one of the following circumstances: on
    first-time access, when the node's last-build time exceeds a stale
    timeout, or on-demand, in that order of precedence.

    Autobuilds are generally enabled if the object has an
    :attr:`autobuild` attribute set. Stale timeout builds require
    :attr:`build_timeout_secs` to be set with with a number value, and
    on-demand builds require :attr:`ondemand`. Missing or negative
    attributes will disable that particular build type.

    On-demand builds are initiated when the called object raises
    :exc:`Autobuilder.OnDemand`. :class:`Autobuilder` will catch this,
    force a node rebuild, and re-call the method once more (any further
    raises of :exc:`Autobuilder.OnDemand` will be ignored). In any mode,
    :exc:`Autobuilder.OnDemand` exceptions are caught and never allowed
    to be raised up the stack. Any build that occurs, whether it is
    forced or from any autobuild, will reset the :attr:`ondemand` flag
    to ``False``, and it must be explictly set back to ``True`` to
    enable future on-demand builds.
    """

    last_build_times = weakref.WeakKeyDictionary()
    build_statuses = weakref.WeakKeyDictionary()

    class OnDemand(Exception):
        pass

    @classmethod
    def get_builder(cls, builder_func):
        # Takes a builder callable and wraps it with a proxy function
        # that updates the build data. All classes need their builders
        # wrapped for proper autobuilding operation, and AutobuildMeta
        # should take care of that using the @builder decorator.
        @wraps(builder_func)
        def _proxy(instance):
            builder_func(instance)
            cls.build_statuses[instance] = True
            cls.last_build_times[instance] = time.time()
            # Clear ondemand attribute, if existing.
            if getattr(instance, "ondemand", False):
                instance.ondemand = False
        return _proxy

    def __init__(self, func, builder_func_name):
        self.target_func = func
        self.builder_func_name = builder_func_name

    def __get__(self, instance, owner):
        # Autobuild descriptor -- returns a wrapper/proxy function that
        # performs autobuilding logic before the method is called.
        # Again, this should be set by AutobuildMeta using the
        # @autobuild decorator.
        if instance is None:
            return self

        @wraps(self.target_func)
        def _wrapper(*args, **kwargs):
            return self._autobuild(instance, *args, **kwargs)

        return _wrapper

    def _autobuild(self, instance, *args, **kwargs):
        builder_func = getattr(instance, self.builder_func_name)

        can_autobuild = getattr(instance, "autobuild", False)
        build_timeout_secs = getattr(instance, "build_timeout_secs", None)
        last_build_time = Autobuilder.last_build_times.get(instance, 0)
        built = Autobuilder.build_statuses.get(instance, False)

        elapsed_time = time.time() - last_build_time

        def _trap_ondemand_call():
            # Utility function that calls the target function and
            # traps/ignores OnDemand exceptions.
            try:
                return self.target_func(instance, *args, **kwargs)
            except Autobuilder.OnDemand:
                return None

        if not can_autobuild:
            return _trap_ondemand_call()

        # First-time and stale-timeout build and call.
        if (not built or
                (build_timeout_secs and elapsed_time >= build_timeout_secs)):
            builder_func()
            return _trap_ondemand_call()
        # Standard non-build call with possible on-demand building if
        # requested and available.
        else:
            for _ in range(2):
                try:
                    return self.target_func(instance, *args, **kwargs)
                except Autobuilder.OnDemand:
                    if getattr(instance, "ondemand", False):
                        builder_func()
                        instance.ondemand = False
                    else:
                        break
            return None


def autobuild(func):
    """Identify a method as an autobuilder, one that requires the object
    to be built before it is executed. The builder is defined via
    :func:`builder`. Requires :class:`AutobuildMeta` to have any effect.
    """

    func._ab_autobuild = True
    return func


def builder(func):
    """Identify a method as the class builder. Methods decorated by
    :func:`autobuild` can automatically call the builder when they need
    to build. Requires :class:`AutobuildMeta` to have any effect.
    """

    func._ab_builder = True
    return func



###############################################################################
# BASE NODE CLASSES
###############################################################################

class Node(object, metaclass=AutobuildMeta):
    """Base node class implementing common attributes and methods. It is
    the ancestor of all :mod:`.nodes` filesystem nodes.

    :param str identity: The node's name.
    """

    #: The length of time required before the node is considered stale
    #: and needs to be rebuilt. This is a class attribute and should not
    #: be defined on instances. By default, it is ``None``, meaning
    #: it will never go stale. Derived classes should set this as
    #: necessary.
    build_timeout_secs = None

    def __init__(self, identity):
        self._identity = None
        self.identity = identity

        #: The node's owning parent. Ordinarily, this should not be
        #: directly set.
        self.parent = None

        self.autobuild = False
        self.ondemand = False

    @property
    def root(self):
        if self.parent:
            return self.parent.root
        else:
            return self

    @property
    def identity(self):
        """The node's name property. When set, the name will be
        normalized. The default behavior is to replace all spaces to
        underscores.
        """

        return self._identity

    @identity.setter
    def identity(self, value):
        self._identity = str(value).replace(" ", "_")

    @builder
    def build(self):
        """Build or rebuild the node. It is called internally whenever
        the node needs to rebuild, but it may be explictly called to
        force rebuilding. Any derived forms of
        :exc:`lutopu.exceptions.BuildError` may be raised if a critical
        error stops the build.
        """

    def __repr__(self):
        return "<{}:{}>".format(self.__class__.__name__, self.identity)


class NodeCollection(object):
    """A container for collecting, indexing, and iterating node objects.
    """

    def __init__(self, parent):
        self._nodes = []
        self._parent = parent
        self.assign_attrs = []

    def append(self, child):
        self._nodes.append(child)
        child.parent = self._parent
        for name in self.assign_attrs:
            setattr(child, name, getattr(self._parent, name, None))

    def remove(self, child):
        self._nodes.remove(child)

    def clear(self):
        # Not strictly necessary, but let's be thorough.
        for node in self._nodes:
            node.parent = None

        del self._nodes[:]

    def find(self, identity):
        try:
            return next((subnode for subnode in self._nodes
                        if subnode.identity == identity))
        except StopIteration:
            return None

    def __iter__(self):
         # yield from self._nodes
        for child in self._nodes:
            yield child

    def __getitem__(self, index):
        return self._nodes[index]

    def __len__(self):
        return len(self._nodes)

    def __contains__(self, value):
        return value in self._nodes

    def __repr__(self):
        return self._nodes.__repr__()


class TreeNode(Node):
    """Base class for nodes forming a tree."""

    def __init__(self, identity):
        super().__init__(identity)
        self._subnodes = NodeCollection(self)
        self._subnodes.assign_attrs = ['autobuild', 'ondemand']

    @autobuild
    def subnodes(self):
        """Collection of branch and leaf subnodes."""

        return self._subnodes

    @autobuild
    def find_subnode(self, identity):
        """Find a subnode of a given *identity*."""

        node = self._subnodes.find(identity)
        if not node:
            raise Autobuilder.OnDemand()
        return node

    def add_subnode(self, node):
        self._subnodes.append(node)

    def remove_subnode(self, node):
        self._subnodes.remove(node)


    def is_leaf(self):
        return len(self._subnodes) == 0

    def walk(self):
        yield self
        for child in self.subnodes():
            try:
                # yield from child.walk()
                for node in child.walk():
                    yield node
            except AttributeError:
                yield child

    @contextlib.contextmanager
    def autobuilding(self, ondemand=False):
        """Context manager that engages and then disengages autobuilds
        at a branch rooted at this node. The optional *ondemand*
        parameter will also enable on-demand build mode for relevant
        methods.
        """

        # Prepare a list of branch nodes instead of iterating on walk()
        # directly, because if we set autobuild during iteration, that
        # can cause subnodes to immediately start building (walk()
        # accesses subnodes(), which is an @autobuild method). This will
        # cascade first-time builds for the entire branch, which we
        # obviously don't want.

        for subnode in list(self.walk()):
            subnode.autobuild = True
            subnode.ondemand = ondemand
        try:
            yield self
        finally:
            for subnode in self.walk():
                subnode.autobuild = subnode.ondemand = False
