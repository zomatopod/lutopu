import logging
import tkinter
from tkinter import ttk, messagebox

from lutopu import net
from lutopu.net import ConnectionState
from lutopu.net import TransferStatus


_logger = logging.getLogger(__name__)


def after_idle(func):
    def _wrapper(self, *args, **kwargs):
        self.after_idle(func, self, *args, **kwargs)
    return _wrapper


def after(ms):
    def decorator(func):
        def _wrapper(self, *args, **kwargs):
            self.after(ms, func, self, *args, **kwargs)
        return _wrapper
    return decorator


def start_application():
    root = tkinter.Tk()
    app = Application(root)
    root.geometry("800x600")
    root.mainloop()


class Application(ttk.Frame):
    def __init__(self, master=None):
        super().__init__(master)

        # Build UI

        # The application frame itself.
        self.master.title("put.io")
        self.pack(expand=1, fill="both")

        # Main sub-frames.
        self.fetch_frames = []
        self.fetch_frames.append(Transfers(master=None))

        # Set the sub-frames into a notebook.
        self.notebook = ttk.Notebook(self)
        for subframe in self.fetch_frames:
            self.notebook.add(subframe, text=subframe.__class__.__name__)
        self.notebook.pack(expand=1, fill="both")

        # Login status
        self.login_widget = Login(self)
        self.login_widget.pack()

        # Disk space progress bar.
        self.disk_space = DiskSpace(self, width=150, height=19)
        self.disk_space.place(anchor="ne", relx=1.0, rely=0.0)

        self.login_status = ConnectionState.disconnected

    def start_fetches(self):
        for frame in self.fetch_frames:
            frame.start()

    def stop_fetches(self):
        for frame in self.fetch_frames:
            frame.stop()

    def login(self, username, password, status_callback=None):
        status_callback = status_callback if status_callback else lambda s,e: None
        _logger.info("Starting login")

        def login_response(data, error):
            if error:
                assert(net.status == ConnectionState.disconnected)
                self.login_status = ConnectionState.disconnected
                messagebox.showerror("o snapz", error)
            else:
                assert(net.status == ConnectionState.connected)
                self.login_status = ConnectionState.connected
                self.start_fetches()

            status_callback(self.login_status)

        if self.login_status == ConnectionState.disconnected:
            assert(net.status == ConnectionState.disconnected)
            net.connect(username, password, callback=login_response)

            self.login_status = ConnectionState.connecting
            status_callback(self.login_status)
        else:
            _logger.warning("Must be disconnected to start a connection")

    def logoff(self, status_callback=None):
        status_callback = status_callback if status_callback else lambda s: None
        _logger.info("Logging off")

        def logoff_response(data, error):
            assert(net.status == ConnectionState.disconnected)
            self.login_status = ConnectionState.disconnected
            self.stop_fetches()
            status_callback(self.login_status)

        if self.login_status == ConnectionState.connected:
            assert(net.status == ConnectionState.connected)
            net.disconnect(callback=logoff_response)

            self.login_status = ConnectionState.disconnecting
            status_callback(self.login_status)
        else:
            _logger.warning("Must be connected to disconnect")


class Login(ttk.Frame):
    def __init__(self, application):
        super().__init__(master=application)

        self.login_username = ""
        self.login_password = ""
        self.username = None

        # Build UI

        self.login_status_message = ttk.Label(text="Status message.")
        self.login_status_message.pack(side="left")

        self.loginoff = ttk.Button(text="Login Button", command=self.log_in_off)
        self.loginoff.pack(side="right")

        self.set_status(ConnectionState.disconnected)

    def log_in_off(self):
        if self.master.login_status == ConnectionState.disconnected:
            self.master.login(self.login_username, self.login_password, self.set_status)

        elif self.master.login_status == ConnectionState.connected:
            self.master.logoff(self.set_status)

    @after_idle
    def set_status(self, stat):
        if stat == ConnectionState.disconnected:
            self.login_status_message.config(text="Disconnected.")
            self.loginoff.config(text="Login", state="enabled")
        elif stat == ConnectionState.connecting:
            self.login_status_message.config(text="Connecting...")
            self.loginoff.config(text="Login", state="disabled")
        elif stat == ConnectionState.connected:
            self.login_status_message.config(
                text="Connected as {} ({}).".format(
                    self.username, self.login_username))
            self.loginoff.config(text="Logoff", state="enabled")
        elif stat == ConnectionState.disconnecting:
            self.login_status_message.config(text="Disconnecting...")
            self.loginoff.config(text="Logoff", state="disabled")


class DiskSpace(tkinter.Canvas):
    HIGH_COLOR = "#696DC8"
    MED_COLOR = "#C8C669"
    LOW_COLOR = "#C87469"

    def __init__(self, master=None, width=100, height=10):
        super().__init__(master, width=width+4, height=height,
                         borderwidth=0, highlightthickness=1)

        self.width = width
        self.height = height

        self.bar = self.create_rectangle(1, 1, width+1, height,  width=1)
        self.progress = a = self.create_rectangle(1, 1, width+1, height, width=1)

        self.label = self.create_text((width*0.5)+1, height*0.5, text="")

    @after_idle
    def set(self, avail, maximum):
        color = None
        percent = avail / maximum
        length = round(percent * self.width)
        text = "{} of {}".format(util.format_bytes(avail), util.format_bytes(maximum))

        self.coords(self.progress, 1, 1, length+1, self.height)
        self.itemconfig(self.label, text=text)

        if percent >= 0.6:
            color = DiskSpace.HIGH_COLOR
        elif 0.3 <= percent < 0.6:
            color = DiskSpace.MED_COLOR
        else:
            color = DiskSpace.LOW_COLOR

        self.itemconfig(self.progress, fill=color)


class FetchFrame(ttk.Frame):
    dc_interval = 250
    interval = 3000

    def __init__(self, update_fetch_fn, update_success_fn=None, master=None):
        super().__init__(master)
        self.config(padding=10)

        self.loop_active = False
        self.alarm_id = 0
        self.update_fetch_fn = update_fetch_fn
        self.update_success_fn = (update_success_fn
            if update_success_fn else lambda d: None)

    def stop(self):
        if self.loop_active:
            self.after_cancel(self.alarm_id)
            self.alarm_id = 0
            self.loop_active = False

    def start(self):
        if not self.loop_active:
            self.loop_active = True
            self._update_loop()

    def _update_loop(self):
        def data_callback(data, error):
            # Between the time when the update function was issued and
            # this callback was invoked when it finished, stop() may
            # have been called. We must check for that first and halt,
            # otherwise the loop will improperly continue.
            if not self.loop_active:
                return

            if error:
                self.alarm_id = self.after(self.dc_interval,
                                           self._update_loop)
            else:
                self.update_success_fn(data)
                self.alarm_id = self.after(self.interval,
                                           self._update_loop)

        if net.status == ConnectionState.connected:
            self.update_fetch_fn(callback=data_callback)
        else:
            self.alarm_id = self.after(self.dc_interval,
                                       self._update_loop)


class Transfers(FetchFrame):
    def __init__(self, master=None):
        super().__init__(net.fetch_transfers, self.update_transfers, master)

    @after_idle
    def update_transfers(self, transfers=[]):
        updated_indices = []

        # Update pre-exiting transfer widgets by the put.io transfer id.
        for child in self.winfo_children():
            for i, transfer in enumerate(transfers):
                if transfer.id == child.id:
                    child.modify(transfer)
                    updated_indices.append(i)
                    break
            # Transfer widget represents a transfer that no longer exists.
            else:
                child.destroy()

        # Add widgets for new transfers (i.e. those that have NOT updated a child
        # in the loop above).
        for transfer in [e for i,e in enumerate(transfers) if i not in updated_indices]:
            new_transfer_item = TransferItem(self, transfer)
            new_transfer_item.pack(fill="x")

        _logger.info("Updated transfers frame")


class TransferItem(ttk.Frame):
    def __init__(self, master, transfer):
        super().__init__(master)
        self.id = transfer.id

        # Build UI (3 rows, 2 columns)

        self.name = ttk.Label(self, font=("","12",""))
        self.name.grid(row=0, columnspan=2,sticky='w')

        self.progress = ttk.Progressbar(self)
        self.progress.grid(row=1, columnspan=2, sticky='ew') # Sticky 'ew' stretches the bar across the specified columns

        self.login_status = ttk.Label(self)
        self.login_status.grid(row=2, column=0, sticky='w')

        self.time = ttk.Label(self)
        self.time.grid(row=2, column=1, sticky='e')

        self.sep = ttk.Separator(self)
        self.sep.grid(row=3, columnspan=2, sticky='ew', pady=8)

        # Allow horizontal stretching
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)

        self.modify(transfer)

    def modify(self, transfer):
        self.name.config(text=transfer.name)
        self.progress.config(maximum=transfer.size, value=transfer.downloaded)
        self.time.config(text=transfer.estimated_time_str if transfer.status == TransferStatus.downloading else "")
        self.login_status.config(text=transfer.status_message)

        # Hide progressbar for finished (not seeding) transfers.
        if transfer.status == TransferStatus.completed:
            self.progress.grid_remove()

        # Set exactly the amount of horizontal space necessary to display est. time information in its column.
        self.columnconfigure(1, minsize=self.time.winfo_width())
