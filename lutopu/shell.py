import getpass
import cmd
import logging
import contextlib
import math
from shlex import shlex

from lutopu import net
from lutopu import config
from lutopu import util
from lutopu import nodes
from lutopu.nodes.putio import PutioRoot
from lutopu.exceptions import (
    PointerError,
    BuildError,
    RequestError,
    NetError
)


_logger = logging.getLogger(__name__)


def start_shell():
    Shell().cmdloop()


class Shell(cmd.Cmd):
    default_username = "lutopu"

    def __init__(self):
        super().__init__()
        self.root = PutioRoot()

        self.pointer = nodes.Pointer(self.root)
        self.username = Shell.default_username

        self.postcmd(False, "")     # To activiate prompt

        # By default, readline delimits the following for autocomplete:
        #       " \n\t`~!@#$%^&*()-=+[{]}\\|;:\'\",<>/?"
        # Since paths are delimited with the separator (forward slash)
        # and thus a valid filename can include the bulk of those
        # characters, we'll change it.
        try:
            import readline
            readline.set_completer_delims(" \t\n" + nodes.SEPARATOR)
        # Windows will fail here.
        except ImportError:
            pass

    @contextlib.contextmanager
    def handle_errors(self):
        """Context manager to gracefully handle any errors that may as a
        consequence of shell operations.
        """

        try:
            yield
        # PointerError: Node pointer object failed to traverse because
        #   the file wasn't found.
        # RequestError: e.g. 404 Not Found or 400 Bad Request from net
        #   module. Tried to reference an id that doexn't exist (404) or
        #   doing an illegal op (400, e.g. taking the listing of a file
        #   instead of a directory).
        # ValueError: DirectoryNode raises ValueErrors for its remote
        #   operatiion methdos if you try to pass a node that doesn't
        #   belong to it. This shouldn't happen normally.
        except (PointerError, RequestError, ValueError) as err:
            print_error(err)
        # DirectoryNodes will raise BuildErrors if it no longer exists
        #   remotely or otherwise could not fetch its own information
        #   from put.io.
        except BuildError as err:
            print_error(err)
            self.pointer.current = self.root
            self.root.reset()
        # Catch-all for any other network error out of our control.
        except NetError as err:
            print_error(err)
            self.logoff()

    def logoff(self):
        self.pointer.current = self.root
        self.root.activated = False
        self.username = Shell.default_username
        net.clear_access_token()
        print("Logged off.")


    ### cmd class significant methods and overrides.
    ################################################

    def postcmd(self, stop, line):
        self.prompt = "\n{}\n({}) ".format(self.pointer().identity,
                                           self.username)
        return stop

    def do_exit(self, arg_str):
        return True

    def do_login(self, arg_str):
        """login [username]"""

        if arg_str:
            username = arg_str
            password = getpass.getpass()
        else:
            username = config['Connection']['Username']
            password = config['Connection']['Password']

        if not username:
            self.do_help("login")
            return

        with self.handle_errors():
            net.login(username, password)
            acct_info = net.fetch_acct_info()
        self.root.activated = True
        self.username = acct_info['username']
        print("Logged in as {username} ({mail})".format(**acct_info))

    def do_logoff(self, arg_str):
        self.logoff()

    def do_ls(self, arg_str):
        """ls [path]"""

        try:
            path = nodes.Path(get_arg_tokens(arg_str=arg_str)[0])
        except IndexError:
            path = nodes.Path()

        with self.handle_errors(), self.root.autobuilding(ondemand=True):
            node = self.pointer(path, peek=True)
            node.build()
            print_node_detail(node)

    def do_cd(self, arg_str):
        """cd [path]"""

        try:
            path = nodes.Path(get_arg_tokens(arg_str=arg_str)[0])
        except IndexError:
            path = nodes.Path()

        with self.handle_errors(), self.root.autobuilding(ondemand=True):
            self.pointer(path)

    def do_mkdir(self, arg_str):
        """mkdir DIRECTORY"""

        try:
            path = nodes.Path(get_arg_tokens(arg_str=arg_str)[0])
        except IndexError:
            self.do_help("mkdir")
            return

        head, tail = path.clear_trailing_sep().split()

        with self.handle_errors(), self.root.autobuilding(ondemand=True):
            node = self.pointer(head, peek=True)
            try:
                node.create_directory(tail)
            except AttributeError:
                print_error("Cannot create a directory here")

    def do_rm(self, arg_str):
        """rm FILE/DIRECTORY"""

        try:
            path = nodes.Path(get_arg_tokens(arg_str=arg_str)[0])
        except IndexError:
            self.do_help("rm")
            return

        with self.handle_errors(), self.root.autobuilding(ondemand=True):
            node = self.pointer(path, peek=True)
            try:
                node.parent.delete(node)
            except AttributeError as err:
                print_error("Cannot delete a file/directory here")

    def do_mv(self, arg_str):
        try:
            tokens = get_arg_tokens(arg_str=arg_str)
            src_path = nodes.Path(tokens[0])
            tgt_path = nodes.Path(tokens[1])
        except IndexError:
            self.do_help("mv")
            return

        with self.handle_errors(), self.root.autobuilding(ondemand=True):
            src_node = self.pointer(src_path, peek=True)
            rename = None
            try:
                tgt_node = self.pointer(tgt_path, peek=True)
            except PointerError:
                tgt_path, rename = tgt_path.split()
                tgt_node = self.pointer(tgt_path, peek=True)

            try:
                if tgt_node is not src_node.parent:
                    src_node.parent.move(src_node, tgt_node)
                if rename:
                    src_node.parent.rename(src_node, rename)
            except AttributeError:
                print_error("Cannot move/rename a file/directory here")

    def do_revoke(self, arg_str):
        net.access_token = None

    def complete_cd(self, text, cmd_line, beginidx, endidx):
        return self.autocomplete_path(text, cmd_line, dirs_only=True)

    def complete_ls(self, text, cmd_line, beginidx, endidx):
        return self.autocomplete_path(text, cmd_line)

    def complete_mkdir(self, text, cmd_line, beginidx, endidx):
        return self.autocomplete_path(text, cmd_line)

    def autocomplete_path(self, text, cmd_line, dirs_only=False):
        # Isolate final argument token (of course the only token that
        # can be autocompleted) and treat it as the path to complete.
        try:
            path = nodes.Path(get_arg_tokens(cmd_line=cmd_line)[-1])
        # No command arguments: default to current directory node.
        except IndexError:
            path = nodes.Path()

        target_dir, filename = path.split()
        assert filename == text

        try:
            with self.root.autobuilding():
                peek_node = self.pointer(target_dir, peek=True)
                files = peek_node.subnodes()
        except (PointerError, BuildError):
            return []

        if not text:
            files_compl = [item.identity for item in files]
        else:
            files_compl = [item.identity for item in files
               if item.identity.startswith(text)]

        return files_compl


def print_error(msg):
    print("*** {}".format(msg))


def get_arg_tokens(cmd_line=None, arg_str=None):
    """Parse and return a list of argument tokens from a string.
    Use the *cmd_line* argument for full command line strings that
    contains the entire command and its argument (the first token is
    considered to be the command token and is discarded), or use
    *arg_str* if the string only contains arguments.
    """

    if cmd_line is None and arg_str is None:
        raise ValueError("Either `cmd_line` or `arg_str` must be "
                         "specified")

    lexer = shlex(cmd_line) if cmd_line else shlex(arg_str)
    lexer.whitespace_split = True
    arg_tokens = list(lexer)

    return arg_tokens[1:] if cmd_line else arg_tokens


def print_node_detail(node):
    """Print detailed information about a node. For regular fetch nodes,
    the contents of their :attr:`data` is printed, and for container
    nodes such as directories and transfer roots, their subnodes.
    """

    def content_sort(node):
        return not getattr(node, "subnodes", False), node.identity.lower()

    try:
        for subnode in sorted(node.subnodes(), key=content_sort):
            print_node_line(subnode)
    except AttributeError:
        for name, value in node.data.items():
            print("{}: {}".format(name, value))


def print_node_line(node):
    """Print a single-line representation of a node."""

    strg = ""

    try:
        data_type = node.data_type
        data = node.data
    # Non-fetchdata nodes (directories, transfer root, etc.)
    except AttributeError:
        data_type = data = None

    if data_type == "file":
        file_info = "{} {:>7}".format(
            data['created_at'].strftime("%b %d %Y %H:%M:%S %Z"),
            data['size_str']
        )
        strg = util.fit_line(file_info, node.identity, variable=1)

    elif data_type == "transfer":
        if data['status'] != net.TransferStatus.downloading:
            msg = data['status'].value.replace("_", " ")
        else:
            msg = ""

        progress_gfx = progressbar(data['percent_done'], msg)
        progress = "{:4.0%}[{}]".format(data['percent_done'], progress_gfx)

        if data['availability']:
            down_info = "{:>10} {:>10}/s  {:4.0%} ".format(
                data['downloaded_str'],
                data['down_speed_str'],
                data['availability']
            )
        else:
            down_info = "{:>10} {:>10}/s       ".format(
                data['downloaded_str'],
                data['down_speed_str']
            )

        strg = util.fit_line(progress, node.identity, down_info, variable=1)

    else:
        strg = node.identity

    print(strg)


def progressbar(percent, message="", length=20,
                fill_char="=", empty_char="-"):

    bar_done = math.floor(length*percent)
    gfx = (fill_char*bar_done) + (empty_char*(length-bar_done))

    half_msg_len = int(len(message) * 0.5)
    half_prog_len = int(length * 0.5)
    odd = 1 if len(message) % 2 == 1 else 0

    return (gfx[:half_prog_len - half_msg_len - odd] + message +
            gfx[half_prog_len + half_msg_len:])
