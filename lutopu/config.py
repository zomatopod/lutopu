import configparser
from os import path
from os.path import expanduser


CONFIG_FILENAME = "lutopu.conf"
config = configparser.ConfigParser()

config['Connection'] = {
    "Username": "",
    "Password": ""
}

config['Debug'] = {
    "LogLevel": "40"
}

# Load config from home directory first, then local.
config.read([
    path.join(expanduser("~"), CONFIG_FILENAME),
    CONFIG_FILENAME
])
