__version__ = "0.1dev"

import logging

from lutopu.config import config
from lutopu.exceptions import (
    LutopuException,
    # Node errors.
    NodeError,
    BuildError,
    PointerError,
    # Net Errors.
    NetError,
    UnauthorizedError,
    ServerConnectionError,
    RequestError
)


logging.basicConfig(level=int(config['Debug']['LogLevel']))
