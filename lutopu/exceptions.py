import logging
import inspect



###############################################################################
# BASE
###############################################################################

class LutopuException(Exception):
    """Base for all :mod:`lutopu` exceptions."""


class LoggedException(LutopuException):
    """A generic exception that writes its message to the calling
    module's logger on creation.
    """

    def __init__(self, *args, **kwargs):
        """__init__()"""

        exc_name = self.__class__.__name__

        try:
            message = args[0]
        except IndexError:
            message = "{} exception occured".format(exc_name)

        super().__init__(*args, **kwargs)

        calling_frame = inspect.stack()[1][0]
        calling_module = inspect.getmodule(calling_frame)
        logger = logging.getLogger(calling_module.__name__)
        logger.error("({}) {}".format(exc_name, message))



###############################################################################
# NODE BUILDING
###############################################################################

class NodeError(LoggedException):
    """Base exception for all :mod:`lutopu.nodes` exceptions."""


class BuildError(NodeError):
    """The node failed to build."""


class PointerError(NodeError):
    """:class:`lutopu.nodes.nav.Pointer` failed to select traverse to a
    node.
    """



###############################################################################
# NETWORK
###############################################################################

class NetError(LoggedException):
    """Base exception for all errors for mod:`lutopu.net`. These errors
    are are signficant to :func:`request_callback`. Only exceptions
    derived from this will be passed to callbacks as *error*.
    """


class UnauthorizedError(NetError):
    """Attempted an operation from an incorrect connection state (e.g.
    trying to connect while already connected).
    """

class ServerConnectionError(NetError):
    """Could not connect to the server."""


class RequestError(NetError):
    def __init__(self, data):
        super().__init__(data['error_message'])
        self.error_data = data

    def __getattr__(self, key):
        return self.error_data[key]
