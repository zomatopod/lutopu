import math

from .terminalsize import get_terminal_size


def format_bytes(integer, rounding=2):
    try:
        prefix = ["B", "K", "M", "G"]
        exp = math.floor(min(math.log(integer, 1024), len(prefix)-1))
        size = integer / math.pow(1024, exp)
        return round(size, rounding), prefix[exp]

    except:
        return 0, prefix[0]


def format_date(seconds):
    prefix = [('s', 1), ('m', 60), ('h', 3600), ('d', 86400), ('w', 604800)]
    strg = ""

    for pre, div in prefix[::-1]:
        quotient_floor = math.floor(seconds / div)
        if quotient_floor >= 1.0:
            strg += "{}{} ".format(quotient_floor, pre)
            seconds -= int(quotient_floor * div)

    return strg


def truncate(strg, width=None, trailing="..."):
    """Truncate a string for a given *width*. If truncated, the
    *trailing* string will be appended at its end (defaults to "...").
    *width* defaults to the current terminal width if omitted.
    """

    if width is None:
        width, _ = get_terminal_size()

    strg = str(strg)
    if len(strg) > width:
        strg = strg[:width-len(trailing)] + trailing
    return strg


def fixed_width(strg, width=None, trailing="..."):
    """Truncate or pad a string to an exact character width. Truncation
    is done via :func:`truncate` using the *width* and *trailing*
    parameters.
    """

    if width is None:
        width, _ = get_terminal_size()

    strg = truncate(strg, width, trailing)
    if len(strg) < width:
        strg += " "*(width-len(strg))
    return strg


def fit_line(*strings, width=None, variable=0, padding=1):
    """Fit a series of strings into a given width.

    :func:`fit_line` takes any variable number of string arguments
    and tries to fit them in such a way that they will take up the
    entire *width*. The *variable* parameter specifies which of these
    strings variable-width, which will be stretched or truncated as
    necessary to fit the entire width. *padding* determines the amount
    of space between each string.

    Examples::

        >>> fit_line('kiwi', 'emu', 'potoo', variable=1, width=20)
        'kiwi emu       potoo'
        >>> fit_line('a particularly long string', '#1', width=20)
        'a particularly... #1'
    """

    if width is None:
        avail_width, _ = get_terminal_size()
    else:
        avail_width = width

    strings = list(strings)

    for idx, strg in enumerate(strings):
        if idx != variable:
            avail_width -= len(strg)

    avail_width -= (len(strings) - 1) * padding

    strings[variable] = fixed_width(strings[variable], avail_width)
    return (" "*padding).join(strings)
