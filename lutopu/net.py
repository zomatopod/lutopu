import threading
import queue
import logging
import urllib.parse
from functools import wraps
from enum import Enum

import requests
import dateutil.parser

from lutopu import util
from lutopu.exceptions import (
    NetError,
    UnauthorizedError,
    ServerConnectionError,
    RequestError
)


_logger = logging.getLogger(__name__)
_lock = threading.Lock()

API_URL = "https://put.io/v2/"              # Must end with a slash.
LOGIN_URL = "https://api.put.io/login"      # Must NOT end with a slash.

CLIENT_ID = 1395
REDIRECT_URI = "http://thenullpointers.com"



###############################################################################
# CALLBACK SUPPORT
###############################################################################

def request_callback(callback=None):
    """request_callback([callback])

    The request callback decorator automatically calls the specified
    *callback* on the return of the wrapped function. The callback must
    have the following signature: ``callback_function(data, error)``.

    When the wrapped function exits normally, its return value is passed
    to *callback* as *data*, and *error* is set to ``None``. However, if
    the function raises a :exc:`NetError` exception, it will be caught
    passed it as the *error* argument (*data* becomes ``None``).
    Non-:exc:`NetError` exceptions are re-raised.

    The wrapped function may raise :exc:`AbortCallback`, which stops the
    callback from being called entirely.

    If *callback* is not supplied, then the function behaves normally.

    :param function callback: Target callback function to invoke.
    """

    def decorator(func):
        def _call_callback(data, error):
            try:
                callback(data, error)
            # In case callback function doesn't implement the correct
            # signature. Since the caller will be running a different
            # thread, we don't want to crash it.
            except TypeError:
                _logger.error("`{}` callback function does not "
                              "implement (data, error)".format(callback)
                  )
        def _wrapper(*args, **kwargs):
            try:
                data = func(*args, **kwargs)
            except NetError as err:
                if callback:
                    _call_callback(None, err)
                else:
                    raise
            except _AbortCallback:
                return
            else:
                if callback:
                    _call_callback(data, None)
                else:
                    return data
        return _wrapper
    return decorator


class _AbortCallback(Exception):
    """This exception may raised by a function decorated with
    :func:`request_callback` to signal that the callback should **not**
    be called.
    """



###############################################################################
# CONNECTIVITY SESSION
###############################################################################

access_token = None


def login(login_name, password, callback=None):
    """login(login_name, password[, callback])

    Establish a session to put.io. The connection attempt blocks until a
    connection is established, unless a callback is supplied. The
    account username is returned on success. It may raise or pass the
    :exc:`ServerConnectionError` or :exc:`UnauthorizedError` if the
    login process failed.

    :param str login_name: put.io login name (i.e. email address)
    :param str password: put.io password.
    :param callback: Callback to invoke once the connection
        process is complete.
    """

    # Threadable closure that does the actual login.
    @request_callback(callback)
    def do_login():
        next_uri_parts = (
            "",
            None,
            "/v2/oauth2/authenticate",
            None,
            urllib.parse.urlencode({
                'client_id': CLIENT_ID,
                'redirect_uri': REDIRECT_URI,
                'response_type': "token"
            }),
            None
        )
        login_param = {
            'name': login_name,
            'password': password,
            'next': urllib.parse.urlunparse(next_uri_parts)
        }

        _logger.info("Attempting login with {}".format(login_param['name']))

        try:
            response = requests.post(LOGIN_URL, data=login_param)
        except requests.ConnectionError:
            raise ServerConnectionError("Could not establish connection to "
                                        "put.io.")
        else:
            url = urllib.parse.urlparse(response.url)
            fragments = urllib.parse.parse_qs(url[5])

            try:
                retr_access_token = fragments['access_token'][0]
            except KeyError:
                raise UnauthorizedError("Failed to login.")

            with _lock:
                global access_token
                access_token = retr_access_token

            _logger.info("Retrieved access token")

    if callback:
        # daemon param needs 3.3+
        # threading.Thread(target=login, daemon=True).start()
        login_thread = threading.Thread(target=do_login)
        login_thread.daemon = True
        login_thread.start()
    else:
        return do_login()


def clear_access_token():
    """Clears the access token. A re-login or valid access token is
    required for further network operations.
    """

    with _lock:
        global access_token
        access_token = None

    _logger.info("Access token cleared")



###############################################################################
# NETWORK TASKS
###############################################################################

NET_RETRIES = 5
_nettask_queue = queue.Queue() #pylint: disable=invalid-name

class NetTaskRequestType(Enum):
    get = 0
    post = 1


class NetTask(object):
    def __init__(self, name, url, request_type=NetTaskRequestType.get,
                 callback=None, request_data=None, proc_func=None):
        self.name = name
        self.url = url
        self.request_type = request_type
        self.callback = callback
        self.request_data = request_data
        self.proc_func = proc_func if proc_func else lambda data: data


class NetTaskDispatcher(threading.Thread):
    def __init__(self, task_queue):
        super().__init__()
        self.queue = task_queue
        self.daemon = True
        self.stop = threading.Event()

    def run(self):
        headers = {"Accept": "application/json"}

        while not self.stop.is_set():
            try:
                task = self.queue.get(timeout=0.1)
            except queue.Empty:
                continue

            name = task.name
            url = task.url
            req_type = task.request_type
            req_data = task.request_data

            callback = task.callback
            proc_func = task.proc_func

            @request_callback(callback)
            def _task():
                data = None
                req_data['oauth_token'] = access_token

                if req_type == NetTaskRequestType.get:
                    logstr = "Getting {} (params={}) (try {}/{})"
                else:
                    logstr = "Posting {} (params={}) (try {}/{})"

                for retry in range(NET_RETRIES):
                    _logger.info(
                        logstr.format(name, req_data, retry+1, NET_RETRIES)
                    )
                    try:
                        if req_type == NetTaskRequestType.get:
                            response = requests.get(
                                url, headers=headers, params=req_data
                            )
                        else:
                            response = requests.post(
                                url, headers=headers, data=req_data
                            )
                        data = response.json()
                    # Connection or data failure (.json() raises
                    # ValueError if it could not decode valid JSON).
                    # Fallthrough and retry.
                    except (requests.ConnectionError, ValueError):
                        pass
                    else:
                        if data['status'] == "ERROR":
                            _logger.error("Received `{}` error".format(
                                data['error_type'])
                            )
                            if data['error_type'] == "Unauthorized":
                                raise UnauthorizedError("Not logged in.")
                            raise RequestError(data)

                        _logger.info("Completed {} operation".format(name))
                        return proc_func(data)

                # Connection failures exhausted all retries.
                raise ServerConnectionError(
                    "Connection failed on {} after {} retries.".format(
                        name, retry+1)
                )

            _task()
            self.queue.task_done()


class NetTasker(object):
    name = ""
    endpoint = ""
    request_type = NetTaskRequestType.get

    def __init__(self, task_queue):
        self.queue = task_queue

    def process_data(self, data):
        return data['status']

    def send_task(self, request_data, callback=None):
        task = NetTask(
            name=self.name,
            url=urllib.parse.urljoin(API_URL, self.endpoint),
            request_type=self.request_type,
            request_data=request_data,
            proc_func=self.process_data,
        )

        if callback:
            task.callback = callback
            self.queue.put(task)
        else:
            blocker = queue.Queue()
            task.callback = lambda data, err: blocker.put((data, err))
            self.queue.put(task)
            data, err = blocker.get()

            if err:
                raise err
            return data

    def __call__(self, callback=None):
        return self.send_task({}, callback)



###############################################################################
# PUT.IO NET TASKS
###############################################################################

class _FetchTransfers(NetTasker):
    name = "transfers"
    endpoint = "transfers/list"
    request_type = NetTaskRequestType.get

    def process_data(self, data):
        _logger.info("Retrieved {} transfers".format(len(data['transfers'])))
        return [_process_transfer_data(f) for f in data['transfers']]


class _FetchAcctInfo(NetTasker):
    name = "account information"
    endpoint = "account/info"
    request_type = NetTaskRequestType.get

    def process_data(self, data):
        data = data['info']
        data['data_type'] = "account"
        return data


class _FetchFileList(NetTasker):
    name = "file list"
    endpoint = "files/list"
    request_type = NetTaskRequestType.get

    def process_data(self, data):
        _logger.info("Retrieved {} files".format(len(data['files'])))
        return [_process_file_data(f) for f in data['files']]

    def __call__(self, putio_id, callback=None):
        return self.send_task({'parent_id': putio_id}, callback)


class _SendCreateDirectory(NetTasker):
    name = "directory creation"
    endpoint = "files/create-folder"
    request_type = NetTaskRequestType.post

    def process_data(self, data):
        _logger.info(
            "Created directory `{}` (id:{})".format(
                data['file']['name'],
                data['file']['id']
            )
        )
        return _process_file_data(data['file'])

    def __call__(self, name, root_putio_id, callback=None):
        return self.send_task(
            {'name': name, 'parent_id': root_putio_id},
            callback
        )


class _SendDelete(NetTasker):
    name = "directory/file deletion"
    endpoint = "files/delete"
    request_type = NetTaskRequestType.post

    def __call__(self, putio_id, callback=None):
        return self.send_task({'file_ids': putio_id}, callback)


class _SendMove(NetTasker):
    name = "directory/file move"
    endpoint = "files/move"
    request_type = NetTaskRequestType.post

    def __call__(self, putio_id, target_id, callback=None):
        return self.send_task(
            {'file_ids': putio_id, 'parent_id': target_id},
            callback
        )


class _SendRename(NetTasker):
    name = "directory/file rename"
    endpoint = "files/rename"
    request_type = NetTaskRequestType.post

    def __call__(self, putio_id, name, callback=None):
        return self.send_task(
            {'file_id': putio_id, 'name': name},
            callback
        )


class TransferStatus(Enum):
    """:mod:`enum` of :class:`Transfer` activity status:

        * :attr:`waiting`: Transfer is waiting for disk space to become
          available.
        * :attr:`in_queue`: Transfer is queued because of concurrent
          download limits (currently 15 in put.io).
        * :attr:`downloading`: Active download is in progress.
        * :attr:`completing`: Download has finished and the files are
          being finalized.
        * :attr:`seeding`: The transfer finished and is now seeding.
        * :attr:`completed`: Finished downloading and seeding.
        * :attr:`error`: Transfer error.
    """

    waiting = "WAITING"
    in_queue = "IN_QUEUE"
    downloading = "DOWNLOADING"
    completing = "COMPLETING"
    seeding = "SEEDING"
    completed = "COMPLETED"
    error = "ERROR"
    unknown = 0


def _process_transfer_data(data):
    data['data_type'] = "transfer"

    # Convert status string into an enum.
    try:
        data['status'] = TransferStatus(data['status'])
    except ValueError:
        # Shouldn't happen unless put.io added a new status value we
        # haven't accounted for.
        data['status'] = TransferStatus.unknown

    # Transfers that have been cached by put.io and thus don't need to
    # be downloaded will show that 0B downloaded, which is technically
    # true, but leads to silly suggestions that you have 0% and 0B of
    # the file available. We'll prevent having to make specific checks
    # for this and just set the attribute directly.
    if data['status'] == TransferStatus.completed:
        data['downloaded'] = data['size']

    # Convert from 0-100 integer to 0.0-1.0 float. Derive a more
    # accurate figure for percent done since the int value we get has
    # zero significant digits.
    data['availability'] = (
        data['availability'] * 0.01
        if data['availability'] else None
    )
    data['percent_done'] = (
        min(data['downloaded'], data['size']) / data['size']
        if data['percent_done'] else 0
    )

    # Parse ISO 8601
    data['created_at'] = dateutil.parser.parse(data['created_at'])
    data['finished_at'] = (dateutil.parser.parse(data['finished_at'])
        if data['finished_at'] else None)

    # Add some human-friendly strings for byte size and time
    # values.
    data['down_speed_str'] = _format_bytes(data['down_speed'])
    data['downloaded_str'] = _format_bytes(data['downloaded'])
    data['size_str'] = _format_bytes(data['size'])
    data['up_speed_str'] = _format_bytes(data['up_speed'])
    data['uploaded_str'] = _format_bytes(data['uploaded'])
    data['time_seeding_str'] = util.format_date(data['seconds_seeding'])
    data['estimated_time_str'] = (util.format_date(data['estimated_time'])
        if data['estimated_time'] else None)

    return data


def _process_file_data(data):
    data['data_type'] = "file"
    data['created_at'] = dateutil.parser.parse(data['created_at'])
    data['first_accessed_at'] = dateutil.parser.parse(
        data['first_accessed_at']) if data['first_accessed_at'] else None
    data['size_str'] = _format_bytes(data['size'])
    return data


def _format_bytes(value, rounding=1):
    size, prefix = util.format_bytes(value, rounding)
    return "{}{}".format(size, prefix)



###############################################################################
# API ENDPOINTS
###############################################################################

fetch_acct_info = _FetchAcctInfo(_nettask_queue)
fetch_file_list = _FetchFileList(_nettask_queue)
fetch_transfers = _FetchTransfers(_nettask_queue)
send_create_dir = _SendCreateDirectory(_nettask_queue)
send_delete = _SendDelete(_nettask_queue)
send_move = _SendMove(_nettask_queue)
send_rename = _SendRename(_nettask_queue)


# Account information
"""Represents account information. Available keys:

    * ``username``
    * ``mail``
    * ``plan_expiration_date``: ISO 8601 formatted date/time
    * ``default_subtitle_language``
    * ``disk``: dictionary of the following:

      - ``avail``
      - ``used``
      - ``size``
"""

# File
"""Represents a file or directory. Available attributes:

    * ``content_type``: MIME-type string
    * ``crc32``
    * ``created_at``: ISO 8601 formatted date/time
    * ``first_accessed_at``: ISO 8601 string, otherwise ``None``.
      Directories will never have an access date.
    * ``icon``: URL of the icon image
    * ``id``
    * ``is_mp4_available``: boolean
    * ``is_shared``: boolean
    * ``name``
    * ``opensubtitles_hash``: defaults to ``None``
    * ``parent_id``: The id of the directory this file or
      subdirectory belongs to. The parent id of items at the lowest
      level will be ``0``, representing the root.
    * ``screenshot``: URL of the screenshot image for applicable
      media files, or ``None``.
    * ``size``: in bytes

The following pretty-formatted strings are also available:

    * ``size_str``
"""

# Transfer
"""Represents an active or otherwise uncleared transfer. Available
attributes:

    * ``availability``: value from ``0.0`` to ``1.0``
    * ``callback_url``: defaults to ``None``
    * ``created_at``: ``datetime`` object.
    * ``created_torrent``: boolean
    * ``current_ratio``
    * ``down_speed``: in bytes
    * ``downloaded``: In bytes. Note that this may be higher than
      :attr:`size`.
    * ``error_message``: This message is cached and may contain an
      error string even if there is no current error. Check
      :attr:`status` to determine the actual error state.
    * ``estimated_time``: ``-1`` if there is no estimated time
    * ``extract``: boolean
    * ``file_id``: defaults to ``None``
    * ``finished_at``: ``datetime`` object or ``None`` if the
      transfer has not yet finished
    * ``id``
    * ``is_private``: boolean
    * ``magneturi``: defaults to ``None``
    * ``name``
    * ``peers_connected``
    * ``peers_getting_from_us``
    * ``peers_sending_to_us``
    * ``percent_done``: float from 0.0-1.0
    * ``save_parent_id``
    * ``seconds_seeding``
    * ``size``: in bytes
    * ``source``: defaults to ``None``
    * ``status``: :class:`TransferStatus` enum
    * ``status_message``
    * ``subscription_id``: defaults to ``None``
    * ``torrent_link``: defaults to ``None``
    * ``tracker_message``: defaults to ``None``
    * ``trackers``: list of trackers or ``None``
    * ``type``
    * ``up_speed``: in bytes
    * ``uploaded``: in bytes

The following pretty-formatted strings are also available:

    * ``down_speed_str``
    * ``downloaded_str``
    * ``size_str``
    * ``up_speed_str``
    * ``uploaded_str``
    * ``time_seeding_str``
    * ``estimated_time_str``
"""



###############################################################################
# POST-INIT
###############################################################################

# Start up TaskDispatcher thread.
NetTaskDispatcher(_nettask_queue).start()
