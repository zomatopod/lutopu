import os
import sys
from setuptools import setup, find_packages, Command

import lutopu


required_pkgs = [
    "dateutils>=0.6.6",
    "requests>=2.2.1"
]

# Python 3.4 enum backport package for lower versions.
if sys.hexversion < 0x030400f0:
    required_pkgs.append("enum34>=0.9.23")


class PyTest(Command):
    user_options = []
    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        import sys,subprocess
        errno = subprocess.call([sys.executable, 'runtests.py'])
        raise SystemExit(errno)


def read(fn):
    cwd = os.path.abspath(os.path.dirname(__file__))
    with open(os.path.join(cwd, fn), 'r') as file_:
        return file_.read()


setup(
    name="lutopu",
    version=lutopu.__version__,
    author="Lindy Meas",
    description="Utility for managing put.io.",
    long_description=read("README.rst"),
    packages=find_packages(),
    install_requires=required_pkgs,
    entry_points={
        'console_scripts': 'lutopu = lutopu.shell:start_shell'
    },
    cmdclass={
        'test': PyTest
    }
)